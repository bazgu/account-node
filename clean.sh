#!/bin/bash
cd `dirname $BASH_SOURCE`

if [ ! -f server.pid ]
then
    exit
fi

if ps -p `cat server.pid` > /dev/null
then
    echo 'ERROR: Server is running'
    exit 1
fi

echo 'INFO: Cleaning...'
rm server.pid

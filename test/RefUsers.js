function dump (refUsername) {
    console.log(refUsername)
    refUsers.forEach(refUsername, user => {
        console.log('  ', user.username)
    })
}

var RefUsers = require('../lib/RefUsers.js')

var user1 = { username: 'user1' },
    user2 = { username: 'user2' },
    user3 = { username: 'user3' }

var refUsers = RefUsers()

refUsers.put('refUsername1', user1)
refUsers.put('refUsername1', user2)
refUsers.put('refUsername1', user3)

refUsers.put('refUsername2', user1)
refUsers.put('refUsername2', user2)
refUsers.put('refUsername2', user3)
refUsers.remove('refUsername2', user3)

refUsers.put('refUsername3', user1)
refUsers.put('refUsername3', user2)
refUsers.put('refUsername3', user3)
refUsers.remove('refUsername3', user1)
refUsers.remove('refUsername3', user2)
refUsers.remove('refUsername3', user3)

dump('refUsername1')
dump('refUsername2')
dump('refUsername3')
dump('refUsername4')

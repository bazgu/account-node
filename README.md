Bazgu Account Node
==================

This program **account-node** is one of the nodes in a Bazgu infrastructure.
An account-node deals with a set of user accounts. It stores usernames,
password hashes, profiles, contacts, blocked users and so forth.
The node is responsible for authentication and permissions for sending messages.

Workflow
--------

Front-nodes connect to an account-node for starting a session either by
creating an account or by signing in or by restoring an old session.
Session-nodes connect to the account-node for sending messages.

Configuration
-------------

See `config.js` for details.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.

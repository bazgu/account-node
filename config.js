module.exports = {

    debug_mode: true,
    index: 0,
    listen: {
        port: 7300,
        host: '127.0.0.1',
    },

    configNode: {
        host: '127.0.0.1',
        port: 7600,
    },

}

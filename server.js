process.chdir(__dirname)

require('./lib/LoadConfig.js')(loadedConfig => {

    function dump () {
        for (var i in users) {
            var object = users[i].toStorageObject()
            fs.writeFileSync('dump/' + i, JSON.stringify(object, null, 4))
        }
    }

    function shutdown () {
        dump()
        process.exit()
    }

    var version = process.argv[2]

    var fs = require('fs'),
        url = require('url')

    var Error404Page = require('./lib/Error404Page.js'),
        Log = require('./lib/Log.js'),
        RefUsers = require('./lib/RefUsers.js'),
        RestoreUsers = require('./lib/RestoreUsers.js')

    var chooseAccountNode = require('./lib/ChooseAccountNode.js')(loadedConfig),
        chooseCaptchaNode = require('./lib/ChooseCaptchaNode.js')(loadedConfig),
        chooseSessionNode = require('./lib/ChooseSessionNode.js')(loadedConfig)

    var contactUsers = RefUsers(),
        requestUsers = RefUsers()

    var users = Object.create(null),
        lowerUsers = Object.create(null)

    RestoreUsers(chooseAccountNode, chooseSessionNode,
        users, lowerUsers, contactUsers, requestUsers)

    var pages = Object.create(null)
    pages['/'] = require('./lib/Page/Index.js')
    pages['/node'] = require('./lib/Page/Node.js')(version)
    pages['/accountNode/addReferer'] = require('./lib/Page/AccountNode/AddReferer.js')(users, requestUsers)
    pages['/accountNode/contactOnline'] = require('./lib/Page/AccountNode/ContactOnline.js')(users)
    pages['/accountNode/contactProfile'] = require('./lib/Page/AccountNode/ContactProfile.js')(contactUsers, requestUsers)
    pages['/accountNode/editContactProfile'] = require('./lib/Page/AccountNode/EditContactProfile.js')(users)
    pages['/accountNode/receiveFileMessage'] = require('./lib/Page/AccountNode/ReceiveFileMessage.js')(users)
    pages['/accountNode/receiveTextMessage'] = require('./lib/Page/AccountNode/ReceiveTextMessage.js')(users)
    pages['/accountNode/removeReferer'] = require('./lib/Page/AccountNode/RemoveReferer.js')(users)
    pages['/accountNode/userOffline'] = require('./lib/Page/AccountNode/UserOffline.js')(contactUsers)
    pages['/accountNode/userOnline'] = require('./lib/Page/AccountNode/UserOnline.js')(contactUsers)
    pages['/frontNode/restoreSession'] = require('./lib/Page/FrontNode/RestoreSession.js')(chooseSessionNode, users)
    pages['/frontNode/signIn'] = require('./lib/Page/FrontNode/SignIn.js')(chooseSessionNode, lowerUsers)
    pages['/frontNode/signUp'] = require('./lib/Page/FrontNode/SignUp.js')(chooseAccountNode, chooseCaptchaNode, chooseSessionNode, users, lowerUsers)
    pages['/sessionNode/addContact'] = require('./lib/Page/SessionNode/AddContact.js')(users, contactUsers, requestUsers)
    pages['/sessionNode/changePassword'] = require('./lib/Page/SessionNode/ChangePassword.js')(users)
    pages['/sessionNode/editProfile'] = require('./lib/Page/SessionNode/EditProfile.js')(users)
    pages['/sessionNode/expireSession'] = require('./lib/Page/SessionNode/ExpireSession.js')(users)
    pages['/sessionNode/ignoreRequest'] = require('./lib/Page/SessionNode/IgnoreRequest.js')(users, requestUsers)
    pages['/sessionNode/overrideContactProfile'] = require('./lib/Page/SessionNode/OverrideContactProfile.js')(users)
    pages['/sessionNode/removeContact'] = require('./lib/Page/SessionNode/RemoveContact.js')(users, contactUsers)
    pages['/sessionNode/removeFile'] = require('./lib/Page/SessionNode/RemoveFile.js')(users)
    pages['/sessionNode/removeRequest'] = require('./lib/Page/SessionNode/RemoveRequest.js')(users, requestUsers)
    pages['/sessionNode/sendFileMessage'] = require('./lib/Page/SessionNode/SendFileMessage.js')(users)
    pages['/sessionNode/sendTextMessage'] = require('./lib/Page/SessionNode/SendTextMessage.js')(users)
    pages['/sessionNode/signOut'] = require('./lib/Page/SessionNode/SignOut.js')(users)
    pages['/sessionNode/unwatchPublicProfile'] = require('./lib/Page/SessionNode/UnwatchPublicProfile.js')(users)
    pages['/sessionNode/watchPublicProfile'] = require('./lib/Page/SessionNode/WatchPublicProfile.js')(lowerUsers)
    require('./lib/ScanFiles.js')('files', pages)

    require('./lib/Server.js')((req, res) => {
        Log.http(req.method + ' ' + req.url)
        var parsedUrl = url.parse(req.url, true)
        var page = pages[parsedUrl.pathname]
        if (page === undefined) page = Error404Page
        page(req, res, parsedUrl)
    })

    process.on('SIGINT', shutdown)
    process.on('SIGTERM', shutdown)

    setInterval(dump, 1000 * 60 * 10)

})

var Log = require('./Log.js')

module.exports = (chooseAccountNode, username,
    contactUsername, file, time, token) => {

    var path = '/accountNode/receiveFileMessage' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&fileToken=' + encodeURIComponent(file.token) +
        '&name=' + encodeURIComponent(file.name) +
        '&size=' + encodeURIComponent(file.size) + '&time=' + time +
        '&token=' + encodeURIComponent(token)

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

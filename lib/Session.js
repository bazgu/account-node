var Log = require('./Log.js')

module.exports = (sessionNode, token, group, longTerm,
    nextCheckTime, closeListener, expireListener) => {

    function check () {

        var timeout = setTimeout(() => {

            var path = '/accountNode/checkSession?token=' + encodeURIComponent(token)

            destroyFunction = sessionNode.fetchJson(path, response => {

                if (response === 'INVALID_TOKEN') {
                    closeExpire()
                    return
                }

                if (response !== true) {
                    Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
                    nextCheckTime = Date.now() + 1000 * 10
                    check()
                    return
                }

                nextCheckTime = Date.now() + 1000 * 60
                check()

            }, () => {
                nextCheckTime = Date.now() + 1000 * 10
                check()
            })

        }, nextCheckTime - Date.now())

        destroyFunction = () => {
            clearTimeout(timeout)
        }

    }

    function close () {
        group.remove(token)
        closeListener()
    }

    function closeExpire () {
        close()
        expireListener()
    }

    var destroyFunction
    check()

    var that = {
        group: group,
        longTerm: longTerm,
        close: () => {
            destroyFunction()
            close()
        },
        expire: () => {
            destroyFunction()
            closeExpire()
        },
        toStorageObject: () => {
            return {
                nextCheckTime: nextCheckTime,
                group: group.identifier,
                longTerm: longTerm,
            }
        },
        wake: () => {
            destroyFunction()
            nextCheckTime = Date.now() + 1000 * 60
            check()
        },
    }

    group.add(token, that)

    return that

}

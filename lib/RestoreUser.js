var Log = require('./Log.js'),
    LongSession = require('./LongSession.js'),
    Password = require('./Password.js'),
    PlainObject = require('./PlainObject.js'),
    ReceivingFile = require('./ReceivingFile.js'),
    Session = require('./Session.js'),
    SessionGroup = require('./SessionGroup.js'),
    User = require('./User.js')

module.exports = (chooseAccountNode, chooseSessionNode,
    getGroup, username, storageObject) => {

    var password = storageObject.password,
        passwordKey = Buffer.from(password.key, 'base64'),
        passwordDigest = Buffer.from(password.digest, 'base64')

    var sessions = Object.create(null)
    ;(() => {
        var objects = storageObject.sessions
        Object.keys(objects).forEach(i => {

            var object = objects[i],
                group = getGroup(object.group),
                longTerm = object.longTerm

            ;(() => {
                var accessTime = object.accessTime
                if (accessTime === undefined) return
                object.nextCheckTime = accessTime + 1000 * 60
            })()

            var sessionNode = chooseSessionNode.fromSessionToken(i)
            if (sessionNode === undefined) return

            sessions[i] = Session(sessionNode, i, group, longTerm, object.nextCheckTime, () => {
                user.removeSession(i)
            }, () => {
                if (!longTerm) return
                user.addLongSession(i, LongSession(i, group, Date.now(), () => {
                    user.removeLongSession(i)
                }))
            })

        })
    })()

    var longSessions = Object.create(null)
    ;(() => {
        var objects = storageObject.longSessions
        Object.keys(objects).forEach(i => {

            var object = objects[i],
                group = getGroup(object.group)

            longSessions[i] = LongSession(i, group, object.accessTime, () => {
                user.removeLongSession(i)
            })

        })
    })()

    var files = Object.create(null)
    ;(() => {
        var objects = storageObject.files
        Object.keys(objects).forEach(i => {

            var sessionNode = chooseSessionNode.fromFileToken(i)
            if (sessionNode === undefined) return

            files[i] = ReceivingFile(sessionNode, i, objects[i].nextCheckTime, () => {
                user.removeFile(i)
            })

        })
    })()

    var accountNode = chooseAccountNode.fromUsername(username)
    if (accountNode !== chooseAccountNode.thisNode) {
        Log.fatal('"' + username + '" doesn\'t belong to this node')
    }

    var user = User(chooseAccountNode, chooseSessionNode,
        username, Password(passwordKey, passwordDigest),
        storageObject.profile, storageObject.registerTime,
        PlainObject(storageObject.contacts),
        PlainObject(storageObject.referers),
        PlainObject(storageObject.requests),
        PlainObject(storageObject.ignoreds),
        PlainObject(storageObject.watchers), storageObject.messages,
        sessions, longSessions, files)

    return user

}

module.exports = version => {

    var content = JSON.stringify({
        software: 'account-node',
        version: version,
    })

    return (req, res) => {
        res.setHeader('Content-Type', 'application/json')
        res.end(content)
    }

}

var methods = [{
    path: 'accountNode/addReferer',
    queryString: {
        username: {
            type: 'string',
        },
        refererUsername: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
    },
}, {
    path: 'accountNode/contactOnline',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
    },
}, {
    path: 'accountNode/contactProfile',
    queryString: {
        username: {
            type: 'string',
        },
        contactsFullName: {
            type: 'string',
        },
        contactsEmail: {
            type: 'string',
        },
        contactsPhone: {
            type: 'string',
        },
        contactsTimezone: {
            type: 'number',
        },
        publicFullName: {
            type: 'string',
        },
        publicEmail: {
            type: 'string',
        },
        publicPhone: {
            type: 'string',
        },
        publicTimezone: {
            type: 'number',
        },
    },
}, {
    path: 'accountNode/editContactProfile',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
        requestReturn: {
            type: 'boolean',
        },
    },
}, {
    path: 'accountNode/receiveFileMessage',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        size: {
            type: 'number',
        },
        time: {
            type: 'number',
        },
        token: {
            type: 'string',
        },
        fileToken: {
            type: 'string',
        },
    },
}, {
    path: 'accountNode/receiveTextMessage',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        text: {
            type: 'string',
        },
        time: {
            type: 'number',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'accountNode/removeReferer',
    queryString: {
        username: {
            type: 'string',
        },
        refererUsername: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
    },
}, {
    path: 'accountNode/userOffline',
    queryString: {
        username: {
            type: 'string',
        },
    },
}, {
    path: 'accountNode/userOnline',
    queryString: {
        username: {
            type: 'string',
        },
    },
}, {
    path: 'frontNode/restoreSession',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'frontNode/signIn',
    queryString: {
        username: {
            type: 'string',
        },
        password: {
            type: 'string',
        },
        longTerm: {
            type: 'boolean',
        },
    },
}, {
    path: 'frontNode/signUp',
    queryString: {
        username: {
            type: 'string',
        },
        password: {
            type: 'string',
        },
        captcha_token: {
            type: 'string',
        },
        captcha_value: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
    },
}, {
    path: 'sessionNode/addContact',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
    },
}, {
    path: 'sessionNode/changePassword',
    queryString: {
        username: {
            type: 'string',
        },
        newPassword: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        currentPassword: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/editProfile',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        fullNamePrivacy: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        emailPrivacy: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        phonePrivacy: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
        timezonePrivacy: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/expireSession',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/ignoreRequest',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        requestUsername: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/overrideContactProfile',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        fullName: {
            type: 'string',
        },
        email: {
            type: 'string',
        },
        phone: {
            type: 'string',
        },
        timezone: {
            type: 'number',
        },
    },
}, {
    path: 'sessionNode/removeContact',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/removeFile',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/removeRequest',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
        requestUsername: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/sendFileMessage',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        size: {
            type: 'number',
        },
        token: {
            type: 'string',
        },
        fileToken: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/sendTextMessage',
    queryString: {
        username: {
            type: 'string',
        },
        contactUsername: {
            type: 'string',
        },
        text: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/signOut',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/unwatchPublicProfile',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}, {
    path: 'sessionNode/watchPublicProfile',
    queryString: {
        username: {
            type: 'string',
        },
        token: {
            type: 'string',
        },
    },
}]

var content =
    '<!DOCTYPE html>' +
    '<html>' +
        '<head>' +
            '<title>Account Node Documentation</title>' +
            '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
            '<link rel="stylesheet" type="text/css" href="index.css" />' +
        '</head>' +
        '<body>' +
            '<h1 class="title">Account Node Documentation</h1>' +
            '<div class="index">' +
                '<h2>Method Reference</h2>' +
                '<ul>' +
                    methods.map(method => {
                        return '<li class="method">' +
                            '<a class="link" href="#' + method.path + '">' +
                                method.path +
                            '</a>' +
                        '</li>'
                    }).join('') +
                '</ul>' +
            '</div>' +
            '<div class="content">' +
                methods.map(method => {
                    var queryString = method.queryString
                    return '<h2 id="' + method.path +'">' +
                            method.path +
                        '</h2>' +
                        '<div style="padding-left: 20px">' +
                            '<div>' +
                                '<h3>Query String</h3>' +
                                '<ul>' +
                                    Object.keys(queryString).map(name => {
                                        var item = queryString[name]
                                        return '<li>' +
                                                '&bull; <code>' + name + ' - (' + item.type + ')</code>' +
                                            '</li>'
                                    }).join('') +
                                '</ul>' +
                            '</div>' +
                        '</div>'
                }).join('<br />') +
            '</div>' +
            '<div style="clear: both"></div>' +
        '</body>' +
    '</html>'

module.exports = (req, res) => {
    res.setHeader('Content-Type', 'text/html; charset=UTF-8')
    res.end(content)
}

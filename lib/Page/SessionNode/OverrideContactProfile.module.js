module.exports = app => users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined) {
        request.respond('INVALID_CONTACT_USERNAME')
        return
    }

    let fullName = query.fullName
    if (fullName === undefined) fullName = ''
    else fullName = app.CollapseSpaces(fullName)

    let email = query.email
    if (email === undefined) email = ''
    else email = app.CollapseSpaces(email)

    let phone = query.phone
    if (phone === undefined) phone = ''
    else phone = app.CollapseSpaces(phone)

    user.overrideContactProfile(contactUsername, {
        fullName: fullName,
        email: email,
        phone: phone,
        timezone: app.ParseTimezone(query.timezone, true),
    }, token)
    request.respond(true)

}

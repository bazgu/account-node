var CollapseSpaces = require('../../CollapseSpaces.js'),
    Contact = require('../../Contact.js'),
    IsUsernameValid = require('../../IsUsernameValid.js'),
    Log = require('../../Log.js'),
    ParseTimezone = require('../../ParseTimezone.js')

module.exports = (users, contactUsers, requestUsers) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var contactUsername = query.contactUsername
        if (contactUsername === undefined ||
            !IsUsernameValid(contactUsername) || contactUsername === username) {

            res.end('"INVALID_CONTACT_USERNAME"')
            return

        }

        var token = query.token
        var session = user.getSession(token)
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }
        session.wake()

        var contact = user.getContact(contactUsername)
        if (contact !== undefined) {
            res.end(JSON.stringify(contact))
            return
        }

        var fullName = query.fullName
        if (fullName === undefined) fullName = ''
        else fullName = CollapseSpaces(fullName)

        var email = query.email
        if (email === undefined) email = ''
        else email = CollapseSpaces(email)

        var phone = query.phone
        if (phone === undefined) phone = ''
        else phone = CollapseSpaces(phone)

        var contact = Contact({
            fullName: fullName,
            email: email,
            phone: phone,
            timezone: ParseTimezone(query.timezone, true),
        })

        var requestRemoved = user.addContact(contactUsername, contact, token)
        contactUsers.put(contactUsername, user)
        if (requestRemoved) requestUsers.remove(contactUsername, user)
        Log.info('"' + username + '" added contact "' + contactUsername + '"')
        res.end(JSON.stringify(contact))

    }
}

module.exports = users => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var text = query.text
        if (text === undefined) {
            res.end('"EMPTY_MESSAGE"')
            return
        }

        var token = query.token
        var session = user.getSession(token)
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }
        session.wake()

        var contactUsername = query.contactUsername
        if (user.getContact(contactUsername) === undefined) {
            res.end('"INVALID_CONTACT_USERNAME"')
            return
        }

        res.end(JSON.stringify(
            user.sendTextMessage(contactUsername, text, token)
        ))

    }
}

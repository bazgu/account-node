module.exports = app => (users, contactUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined) {
        request.respond('INVALID_CONTACT_USERNAME')
        return
    }

    user.removeContact(contactUsername, token)
    contactUsers.remove(contactUsername, user)
    app.log.info('"' + username + '" removed contact "' + contactUsername + '"')
    request.respond(true)

}

module.exports = users => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var file = user.getFile(query.token)
        if (file === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        file.destroy()
        res.end('true')

    }
}

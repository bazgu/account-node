module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const file = user.getFile(query.token)
    if (file === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    file.destroy()
    request.respond(true)

}

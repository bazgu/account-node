module.exports = (users, requestUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    const requestUsername = query.requestUsername
    if (user.getRequest(requestUsername) === undefined) {
        request.respond('INVALID_REQUEST_USERNAME')
        return
    }

    user.ignoreRequest(requestUsername, token)
    requestUsers.remove(requestUsername, user)
    request.respond(true)

}

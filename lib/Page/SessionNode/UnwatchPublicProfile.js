module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    if (user.getWatcher(token) === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    user.unwatchPublicProfile(token)
    request.respond(true)

}

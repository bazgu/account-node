module.exports = users => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var token = query.token
        if (user.getWatcher(token) === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        user.unwatchPublicProfile(token)
        res.end('true')

    }
}

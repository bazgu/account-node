module.exports = app => users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const newPassword = query.newPassword
    if (newPassword === undefined || !app.IsPasswordValid(newPassword)) {
        request.respond('INVALID_NEW_PASSWORD')
        return
    }

    const session = user.getSession(query.token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    if (!user.passwordMatches(query.currentPassword)) {
        request.respond('INVALID_CURRENT_PASSWORD')
        return
    }

    user.changePassword(newPassword)
    app.log.info('"' + username + '" changed password')
    request.respond(true)

}

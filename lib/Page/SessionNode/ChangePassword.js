var IsPasswordValid = require('../../IsPasswordValid.js'),
    Log = require('../../Log.js')

module.exports = users => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var newPassword = query.newPassword
        if (newPassword === undefined || !IsPasswordValid(newPassword)) {
            res.end('"INVALID_NEW_PASSWORD"')
            return
        }

        var session = user.getSession(query.token)
        if (session === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }
        session.wake()

        if (!user.passwordMatches(query.currentPassword)) {
            res.end('"INVALID_CURRENT_PASSWORD"')
            return
        }

        user.changePassword(newPassword)
        Log.info('"' + username + '" changed password')
        res.end('true')

    }
}

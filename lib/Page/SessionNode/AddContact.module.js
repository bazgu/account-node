module.exports = app => (users, contactUsers, requestUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const contactUsername = query.contactUsername
    if (contactUsername === undefined ||
        !app.IsUsernameValid(contactUsername) || contactUsername === username) {

        request.respond('INVALID_CONTACT_USERNAME')
        return

    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    const existing_contact = user.getContact(contactUsername)
    if (existing_contact !== undefined) {
        request.respond(existing_contact)
        return
    }

    let fullName = query.fullName
    if (fullName === undefined) fullName = ''
    else fullName = app.CollapseSpaces(fullName)

    let email = query.email
    if (email === undefined) email = ''
    else email = app.CollapseSpaces(email)

    let phone = query.phone
    if (phone === undefined) phone = ''
    else phone = app.CollapseSpaces(phone)

    const contact = app.Contact({
        fullName: fullName,
        email: email,
        phone: phone,
        timezone: app.ParseTimezone(query.timezone, true),
    })

    const requestRemoved = user.addContact(contactUsername, contact, token)
    contactUsers.put(contactUsername, user)
    if (requestRemoved) requestUsers.remove(contactUsername, user)
    app.log.info('"' + username + '" added contact "' + contactUsername + '"')
    request.respond(contact)

}

module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session !== undefined) session.group.close(token)

    request.respond(true)

}

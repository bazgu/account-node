function parsePrivacy (value) {
    if (value !== 'contacts' && value !== 'public') return 'private'
    return value
}

module.exports = app => users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    let fullName = query.fullName
    if (fullName === undefined) fullName = ''
    else fullName = app.CollapseSpaces(fullName)

    let email = query.email
    if (email === undefined) email = ''
    else email = app.CollapseSpaces(email)

    let phone = query.phone
    if (phone === undefined) phone = ''
    else phone = app.CollapseSpaces(phone)

    user.editProfile({
        fullName: fullName,
        fullNamePrivacy: parsePrivacy(query.fullNamePrivacy),
        email: email,
        emailPrivacy: parsePrivacy(query.emailPrivacy),
        phone: phone,
        phonePrivacy: parsePrivacy(query.phonePrivacy),
        timezone: app.ParseTimezone(query.timezone),
        timezonePrivacy: parsePrivacy(query.timezonePrivacy),
    }, token)
    app.log.info('"' + username + '" edited profile')
    request.respond(true)

}

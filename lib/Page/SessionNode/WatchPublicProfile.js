module.exports = lowerUsers => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var token = query.token
        if (token === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var username = query.username
        var user = lowerUsers[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        res.end(JSON.stringify(user.watchPublicProfile(token)))

    }
}

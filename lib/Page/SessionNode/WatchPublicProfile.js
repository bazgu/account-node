module.exports = lowerUsers => request => {

    const query = request.parsed_url.query

    const token = query.token
    if (token === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const username = query.username
    const user = lowerUsers[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    request.respond(user.watchPublicProfile(token))

}

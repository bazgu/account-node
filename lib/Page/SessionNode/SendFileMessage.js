module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const name = query.name
    if (name === undefined) {
        request.respond('INVALID_NAME')
        return
    }

    const size = parseInt(query.size, 10)
    if (!isFinite(size)) {
        request.respond('INVALID_SIZE')
        return
    }

    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }
    session.wake()

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined) {
        request.respond('INVALID_CONTACT_USERNAME')
        return
    }

    request.respond(user.sendFileMessage(contactUsername, {
        token: query.fileToken,
        name: name,
        size: size,
    }, token))

}

module.exports = app => (users, requestUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const refererUsername = query.refererUsername
    if (refererUsername === undefined ||
        !app.IsUsernameValid(refererUsername) || refererUsername === username) {

        request.respond('INVALID_REFERER_USERNAME')
        return

    }

    let fullName = query.fullName
    if (fullName === undefined) fullName = ''
    else fullName = app.CollapseSpaces(fullName)

    let email = query.email
    if (email === undefined) email = ''
    else email = app.CollapseSpaces(email)

    let phone = query.phone
    if (phone === undefined) phone = ''
    else phone = app.CollapseSpaces(phone)

    const requestAdded = user.addReferer(refererUsername, {
        fullName: fullName,
        email: email,
        phone: phone,
        timezone: app.ParseTimezone(query.timezone, true),
    })
    if (requestAdded) requestUsers.put(refererUsername, user)

    request.respond(true)

}

module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined) {
        request.respond('INVALID_CONTACT_USERNAME')
        return
    }

    user.contactOnline(contactUsername)

    request.respond(true)

}

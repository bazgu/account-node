module.exports = app => (contactUsers, requestUsers) => request => {

    const query = request.parsed_url.query

    let contactsFullName = query.contactsFullName
    if (contactsFullName === undefined) contactsFullName = ''
    else contactsFullName = app.CollapseSpaces(contactsFullName)

    let contactsEmail = query.contactsEmail
    if (contactsEmail === undefined) contactsEmail = ''
    else contactsEmail = app.CollapseSpaces(contactsEmail)

    let contactsPhone = query.contactsPhone
    if (contactsPhone === undefined) contactsPhone = ''
    else contactsPhone = app.CollapseSpaces(contactsPhone)

    let publicFullName = query.publicFullName
    if (publicFullName === undefined) publicFullName = ''
    else publicFullName = app.CollapseSpaces(publicFullName)

    let publicEmail = query.publicEmail
    if (publicEmail === undefined) publicEmail = ''
    else publicEmail = app.CollapseSpaces(publicEmail)

    let publicPhone = query.publicPhone
    if (publicPhone === undefined) publicPhone = ''
    else publicPhone = app.CollapseSpaces(publicPhone)

    const contactsProfile = {
        fullName: contactsFullName,
        email: contactsEmail,
        phone: contactsPhone,
        timezone: app.ParseTimezone(query.contactsTimezone, true),
    }

    const publicProfile = {
        fullName: publicFullName,
        email: publicEmail,
        phone: publicPhone,
        timezone: app.ParseTimezone(query.publicTimezone, true),
    }

    const username = query.username
    contactUsers.forEach(username, user => {
        if (user.getReferer(username) === undefined) {
            user.editContactProfile(username, publicProfile)
        } else {
            user.editContactProfile(username, contactsProfile)
        }
    })
    requestUsers.forEach(username, user => {
        user.editRequest(username, contactsProfile)
    })

    request.respond(true)

}

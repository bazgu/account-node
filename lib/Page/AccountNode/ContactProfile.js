var CollapseSpaces = require('../../CollapseSpaces.js'),
    ParseTimezone = require('../../ParseTimezone.js')

module.exports = (contactUsers, requestUsers) => {
    return (req, res, parsedUrl) => {

        var query = parsedUrl.query

        var contactsFullName = query.contactsFullName
        if (contactsFullName === undefined) contactsFullName = ''
        else contactsFullName = CollapseSpaces(contactsFullName)

        var contactsEmail = query.contactsEmail
        if (contactsEmail === undefined) contactsEmail = ''
        else contactsEmail = CollapseSpaces(contactsEmail)

        var contactsPhone = query.contactsPhone
        if (contactsPhone === undefined) contactsPhone = ''
        else contactsPhone = CollapseSpaces(contactsPhone)

        var publicFullName = query.publicFullName
        if (publicFullName === undefined) publicFullName = ''
        else publicFullName = CollapseSpaces(publicFullName)

        var publicEmail = query.publicEmail
        if (publicEmail === undefined) publicEmail = ''
        else publicEmail = CollapseSpaces(publicEmail)

        var publicPhone = query.publicPhone
        if (publicPhone === undefined) publicPhone = ''
        else publicPhone = CollapseSpaces(publicPhone)

        var contactsProfile = {
            fullName: contactsFullName,
            email: contactsEmail,
            phone: contactsPhone,
            timezone: ParseTimezone(query.contactsTimezone, true),
        }

        var publicProfile = {
            fullName: publicFullName,
            email: publicEmail,
            phone: publicPhone,
            timezone: ParseTimezone(query.publicTimezone, true),
        }

        var username = query.username
        contactUsers.forEach(username, user => {
            if (user.getReferer(username) === undefined) {
                user.editContactProfile(username, publicProfile)
            } else {
                user.editContactProfile(username, contactsProfile)
            }
        })
        requestUsers.forEach(username, user => {
            user.editRequest(username, contactsProfile)
        })

        res.setHeader('Content-Type', 'application/json')
        res.end('true')

    }
}

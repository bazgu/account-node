module.exports = contactUsers => {
    return (req, res, parsedUrl) => {

        var username = parsedUrl.query.username
        if (username === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        contactUsers.forEach(username, user => {
            user.contactOffline(username)
        })

        res.setHeader('Content-Type', 'application/json')
        res.end('true')

    }
}

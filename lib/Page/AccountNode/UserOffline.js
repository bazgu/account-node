module.exports = contactUsers => request => {

    const username = request.parsed_url.query.username
    if (username === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    contactUsers.forEach(username, user => {
        user.contactOffline(username)
    })

    request.respond(true)

}

module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const name = query.name
    if (name === undefined) {
        request.respond('INVALID_NAME')
        return
    }

    const size = parseInt(query.size, 10)
    if (!isFinite(size)) {
        request.respond('INVALID_SIZE')
        return
    }

    const time = parseInt(query.time, 10)
    if (!isFinite(time)) {
        request.respond('INVALID_TIME')
        return
    }

    const token = query.token
    if (token === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const fileToken = query.fileToken
    if (fileToken === undefined) {
        request.respond('INVALID_FILE_TOKEN')
        return
    }

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined &&
        user.getRequest(contactUsername) === undefined) {

        request.respond('INVALID_CONTACT_USERNAME')
        return

    }

    user.receiveFileMessage(contactUsername, {
        token: fileToken,
        name: name,
        size: size,
    }, time, token)

    request.respond(true)

}

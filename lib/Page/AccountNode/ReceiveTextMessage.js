module.exports = users => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = users[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        var text = query.text
        if (text === undefined) {
            res.end('"EMPTY_MESSAGE"')
            return
        }

        var time = parseInt(query.time, 10)
        if (!isFinite(time)) {
            res.end('"INVALID_TIME"')
            return
        }

        var token = query.token
        if (token === undefined) {
            res.end('"INVALID_TOKEN"')
            return
        }

        var contactUsername = query.contactUsername
        if (user.getContact(contactUsername) === undefined &&
            user.getRequest(contactUsername) === undefined) {

            res.end('"INVALID_CONTACT_USERNAME"')
            return

        }

        user.receiveTextMessage(contactUsername, text, time, token)
        res.end('true')

    }
}

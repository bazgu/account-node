module.exports = users => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    const text = query.text
    if (text === undefined) {
        request.respond('EMPTY_MESSAGE')
        return
    }

    const time = parseInt(query.time, 10)
    if (!isFinite(time)) {
        request.respond('INVALID_TIME')
        return
    }

    const token = query.token
    if (token === undefined) {
        request.respond('INVALID_TOKEN')
        return
    }

    const contactUsername = query.contactUsername
    if (user.getContact(contactUsername) === undefined &&
        user.getRequest(contactUsername) === undefined) {

        request.respond('INVALID_CONTACT_USERNAME')
        return

    }

    user.receiveTextMessage(contactUsername, text, time, token)
    request.respond(true)

}

var Error500Page = require('../../../Error500Page.js'),
    Log = require('../../../Log.js'),
    LongSession = require('../../../LongSession.js'),
    Session = require('../../../Session.js')

module.exports = (chooseSessionNode, group,
    username, user, res, longTerm, callback) => {

    var sessionNode = chooseSessionNode.random()

    var path = '/accountNode/open?username=' + encodeURIComponent(username)
    if (longTerm) path += '&longTerm=true'
    path += '&prefix=' + sessionNode.index

    sessionNode.fetchJson(path, token => {
        user.addSession(token, Session(sessionNode, token, group, longTerm, Date.now() + 1000 * 60, () => {
            user.removeSession(token)
        }, () => {
            if (!longTerm) return
            user.addLongSession(token, LongSession(token, group, Date.now(), () => {
                user.removeLongSession(token)
            }))
        }))
        callback({
            token: token,
            profile: user.getProfile(),
            contacts: user.getContacts(),
            requests: user.getRequests(),
            files: user.getFiles(),
            messages: user.pullMessages(),
        })
    }, () => {
        Error500Page(res)
    })

}

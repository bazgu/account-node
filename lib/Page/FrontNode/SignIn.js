var http = require('http')

var NewSessionGroup = require('../../NewSessionGroup.js'),
    OpenSession = require('./lib/OpenSession.js')

module.exports = (chooseSessionNode, lowerUsers) => {
    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        var user = lowerUsers[username]
        if (user === undefined) {
            res.end('"INVALID_USERNAME"')
            return
        }

        if (!user.passwordMatches(query.password)) {
            res.end('"INVALID_PASSWORD"')
            return
        }

        OpenSession(chooseSessionNode, NewSessionGroup(chooseSessionNode),
            user.username, user, res, Boolean(query.longTerm), clientSession => {

            res.end(JSON.stringify({
                username: user.username,
                session: clientSession,
            }))

        })

    }
}

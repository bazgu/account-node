var Error500Page = require('../../Error500Page.js'),
    IsPasswordValid = require('../../IsPasswordValid.js'),
    IsUsernameValid = require('../../IsUsernameValid.js'),
    Log = require('../../Log.js'),
    NewSessionGroup = require('../../NewSessionGroup.js'),
    NewUser = require('../../NewUser.js'),
    OfferUsername = require('../../OfferUsername.js'),
    OpenSession = require('./lib/OpenSession.js'),
    ParseTimezone = require('../../ParseTimezone.js')

module.exports = (chooseAccountNode,
    chooseCaptchaNode, chooseSessionNode, users, lowerUsers) => {

    return (req, res, parsedUrl) => {

        res.setHeader('Content-Type', 'application/json')

        var query = parsedUrl.query

        var username = query.username
        if (username === undefined || !IsUsernameValid(username)) {

            res.end('"INVALID_USERNAME"')
            return

        }

        var password = query.password
        if (password === undefined || !IsPasswordValid(password)) {
            res.end('"INVALID_PASSWORD"')
            return
        }

        var captcha_token = query.captcha_token
        if (captcha_token === undefined) {
            res.end('"INVALID_CAPTCHA_TOKEN"')
            return
        }

        var captchaNode = chooseCaptchaNode(captcha_token)
        if (captchaNode === undefined) {
            res.end('"INVALID_CAPTCHA_TOKEN"')
            return
        }

        var captcha_value = query.captcha_value
        if (captcha_value === undefined) {
            res.end('"INVALID_CAPTCHA_VALUE"')
            return
        }

        var lowerUsername = username.toLowerCase()
        if (lowerUsers[lowerUsername] !== undefined) {
            res.end(JSON.stringify({
                error: 'USERNAME_UNAVAILABLE',
                offerUsername: OfferUsername(chooseAccountNode, username, lowerUsers),
            }))
            return
        }

        ;(() => {

            var path = '/accountNode/verify' +
                '?token=' + encodeURIComponent(captcha_token) +
                '&value=' + encodeURIComponent(captcha_value) +
                '&prefix=' + captchaNode.index

            captchaNode.fetchJson(path, response => {

                if (response.error === 'INVALID_TOKEN') {
                    res.end(JSON.stringify({
                        error: 'INVALID_CAPTCHA_TOKEN',
                        newCaptcha: response.newCaptcha,
                    }))
                    return
                }

                if (response === 'INVALID_VALUE') {
                    res.end('"INVALID_CAPTCHA_VALUE"')
                    return
                }

                if (response !== true) {
                    Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
                    Error500Page(res)
                    return
                }

                var user = NewUser(chooseAccountNode, chooseSessionNode,
                    username, password, ParseTimezone(query.timezone))

                users[username] = user
                lowerUsers[lowerUsername] = user

                Log.info('"' + username + '" signed up')
                OpenSession(chooseSessionNode, NewSessionGroup(chooseSessionNode), username, user, res, true, clientSession => {
                    res.end(JSON.stringify(clientSession))
                })

            }, () => {
                Error500Page(res)
            })

        })()

    }

}

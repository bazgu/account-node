module.exports = app => (chooseSessionNode, lowerUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = lowerUsers[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    if (!user.passwordMatches(query.password)) {
        request.respond('INVALID_PASSWORD')
        return
    }

    app.OpenSession(chooseSessionNode, app.NewSessionGroup(chooseSessionNode),
        user.username, user, request, Boolean(query.longTerm), clientSession => {

        request.respond({
            username: user.username,
            session: clientSession,
        })

    })

}

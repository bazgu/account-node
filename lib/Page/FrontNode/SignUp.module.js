module.exports = app => (chooseAccountNode,
    chooseCaptchaNode, chooseSessionNode, users, lowerUsers) => request => {

    const query = request.parsed_url.query

    const username = query.username
    if (username === undefined || !app.IsUsernameValid(username)) {

        request.respond('INVALID_USERNAME')
        return

    }

    const password = query.password
    if (password === undefined || !app.IsPasswordValid(password)) {
        request.respond('INVALID_PASSWORD')
        return
    }

    const captcha_token = query.captcha_token
    if (captcha_token === undefined) {
        request.respond('INVALID_CAPTCHA_TOKEN')
        return
    }

    const captchaNode = chooseCaptchaNode(captcha_token)
    if (captchaNode === undefined) {
        request.respond('INVALID_CAPTCHA_TOKEN')
        return
    }

    const captcha_value = query.captcha_value
    if (captcha_value === undefined) {
        request.respond('INVALID_CAPTCHA_VALUE')
        return
    }

    const lowerUsername = username.toLowerCase()
    if (lowerUsers[lowerUsername] !== undefined) {
        request.respond({
            error: 'USERNAME_UNAVAILABLE',
            offerUsername: app.OfferUsername(chooseAccountNode, username, lowerUsers),
        })
        return
    }

    ;(() => {

        const logPrefix = captchaNode.logPrefix + 'accountNode/verify: '

        const path = '/accountNode/verify' +
            '?token=' + encodeURIComponent(captcha_token) +
            '&value=' + encodeURIComponent(captcha_value) +
            '&prefix=' + captchaNode.index

        captchaNode.fetchJson(path, response => {

            if (response.error === 'INVALID_TOKEN') {
                request.respond({
                    error: 'INVALID_CAPTCHA_TOKEN',
                    newCaptcha: response.newCaptcha,
                })
                return
            }

            if (response === 'INVALID_VALUE') {
                request.respond('INVALID_CAPTCHA_VALUE')
                return
            }

            if (response !== true) {
                app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
                app.Error500Page(request)
                return
            }

            const user = app.NewUser(chooseAccountNode, chooseSessionNode,
                username, password, app.ParseTimezone(query.timezone))

            users[username] = user
            lowerUsers[lowerUsername] = user

            app.log.info('"' + username + '" signed up')
            app.OpenSession(chooseSessionNode, app.NewSessionGroup(chooseSessionNode), username, user, request, true, clientSession => {
                request.respond(clientSession)
            })

        }, () => {
            app.Error500Page(request)
        })

    })()

}

module.exports = app => (chooseSessionNode, users) => request => {

    const query = request.parsed_url.query

    const username = query.username
    const user = users[username]
    if (user === undefined) {
        request.respond('INVALID_USERNAME')
        return
    }

    let longTerm, group
    const token = query.token
    const session = user.getSession(token)
    if (session === undefined) {
        const longSession = user.getLongSession(token)
        if (longSession === undefined) {
            request.respond('INVALID_TOKEN')
            return
        }
        longTerm = true
        group = longSession.group
    } else {
        longTerm = session.longTerm
        group = session.group
    }

    app.OpenSession(chooseSessionNode, group, username, user, request, longTerm, clientSession => {
        request.respond(clientSession)
    })

}

var Log = require('./Log.js')

module.exports = (chooseAccountNode, contacts,
    referers, username, contactsProfile, publicProfile) => {

    var nodes = []
    for (var i in contacts) {
        var accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }
    for (var i in referers) {
        var accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }

    nodes.forEach(fetchJson => {

        var path = '/accountNode/contactProfile' +
            '?username=' + encodeURIComponent(username) +
            '&contactsFullName=' + encodeURIComponent(contactsProfile.fullName) +
            '&contactsEmail=' + encodeURIComponent(contactsProfile.email) +
            '&contactsPhone=' + encodeURIComponent(contactsProfile.phone)

        var contactsTimezone = contactsProfile.timezone
        if (contactsTimezone !== null) {
            path += '&contactsTimezone=' + contactsTimezone
        }

        path += '&publicFullName=' + encodeURIComponent(publicProfile.fullName) +
            '&publicEmail=' + encodeURIComponent(publicProfile.email) +
            '&publicPhone=' + encodeURIComponent(publicProfile.phone)

        var publicTimezone = publicProfile.timezone
        if (publicTimezone !== null) {
            path += '&publicTimezone=' + publicTimezone
        }

        fetchJson(path, response => {
            if (response === true) return
            Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        }, () => {})

    })

}

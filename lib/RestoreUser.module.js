module.exports = app => (chooseAccountNode, chooseSessionNode,
    getGroup, username, storageObject) => {

    const password = storageObject.password,
        passwordKey = Buffer.from(password.key, 'base64'),
        passwordDigest = Buffer.from(password.digest, 'base64')

    const sessions = Object.create(null)
    ;(() => {
        const objects = storageObject.sessions
        Object.keys(objects).forEach(i => {

            const object = objects[i],
                group = getGroup(object.group),
                longTerm = object.longTerm

            ;(() => {
                const accessTime = object.accessTime
                if (accessTime === undefined) return
                object.nextCheckTime = accessTime + 1000 * 60
            })()

            const sessionNode = chooseSessionNode.fromSessionToken(i)
            if (sessionNode === undefined) return

            sessions[i] = app.Session(sessionNode, i, group, longTerm, object.nextCheckTime, () => {
                user.removeSession(i)
            }, () => {
                if (!longTerm) return
                user.addLongSession(i, app.LongSession(i, group, Date.now(), () => {
                    user.removeLongSession(i)
                }))
            })

        })
    })()

    const longSessions = Object.create(null)
    ;(() => {
        const objects = storageObject.longSessions
        Object.keys(objects).forEach(i => {

            const object = objects[i],
                group = getGroup(object.group)

            longSessions[i] = app.LongSession(i, group, object.accessTime, () => {
                user.removeLongSession(i)
            })

        })
    })()

    const files = Object.create(null)
    ;(() => {
        const objects = storageObject.files
        Object.keys(objects).forEach(i => {

            const sessionNode = chooseSessionNode.fromFileToken(i)
            if (sessionNode === undefined) return

            files[i] = app.ReceivingFile(sessionNode, i, objects[i].nextCheckTime, () => {
                user.removeFile(i)
            })

        })
    })()

    const accountNode = chooseAccountNode.fromUsername(username)
    if (accountNode !== chooseAccountNode.thisNode) {
        app.CliError('"' + username + '" doesn\'t belong to this node')
    }

    const user = app.User(chooseAccountNode, chooseSessionNode,
        username, app.Password(passwordKey, passwordDigest),
        storageObject.profile, storageObject.registerTime,
        app.PlainObject(storageObject.contacts),
        app.PlainObject(storageObject.referers),
        app.PlainObject(storageObject.requests),
        app.PlainObject(storageObject.ignoreds),
        app.PlainObject(storageObject.watchers), storageObject.messages,
        sessions, longSessions, files)

    return user

}

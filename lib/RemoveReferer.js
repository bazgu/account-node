var Log = require('./Log.js')

module.exports = (chooseAccountNode, contactUsername, username, publicProfile) => {

    var path = '/accountNode/removeReferer' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&refererUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(publicProfile.fullName) +
        '&email=' + encodeURIComponent(publicProfile.email) +
        '&phone=' + encodeURIComponent(publicProfile.phone)

    var timezone = publicProfile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

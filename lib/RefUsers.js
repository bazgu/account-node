module.exports = () => {
    const refs = Object.create(null)
    return {
        forEach (refUsername, callback) {

            const ref = refs[refUsername]
            if (ref === undefined) return

            const users = ref.users
            for (const i in users) callback(users[i])

        },
        put (refUsername, user) {

            let ref = refs[refUsername]
            if (ref === undefined) {
                ref = refs[refUsername] = {
                    users: Object.create(null),
                    size: 0,
                }
            }

            const users = ref.users
            const username = user.username
            if (users[username] !== undefined) return

            users[username] = user
            ref.size++

        },
        remove (refUsername, user) {

            const ref = refs[refUsername]
            if (ref === undefined) return

            const users = ref.users
            const username = user.username
            if (users[username] === undefined) return

            delete users[user.username]
            ref.size--
            if (ref.size === 0) delete refs[refUsername]

        },
    }
}

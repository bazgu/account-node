module.exports = () => {
    var refs = Object.create(null)
    return {
        forEach: (refUsername, callback) => {

            var ref = refs[refUsername]
            if (ref === undefined) return

            var users = ref.users
            for (var i in users) callback(users[i])

        },
        put: (refUsername, user) => {

            var ref = refs[refUsername]
            if (ref === undefined) {
                ref = refs[refUsername] = {
                    users: Object.create(null),
                    size: 0,
                }
            }

            var users = ref.users
            var username = user.username
            if (users[username] !== undefined) return

            users[username] = user
            ref.size++

        },
        remove: (refUsername, user) => {

            var ref = refs[refUsername]
            if (ref === undefined) return

            var users = ref.users
            var username = user.username
            if (users[username] === undefined) return

            delete users[user.username]
            ref.size--
            if (ref.size === 0) delete refs[refUsername]

        },
    }
}

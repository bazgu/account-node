module.exports = app => (chooseSessionNode, tokens) => {

    const indexes = Object.create(null)
    tokens.forEach(token => {

        const node = chooseSessionNode.fromSessionToken(token)
        if (node === undefined) return

        const index = node.index
        let set = indexes[index]
        if (set === undefined) {
            set = indexes[index] = {
                node: node,
                tokens: [],
            }
        }
        set.tokens.push(token)

    })

    Object.keys(indexes).forEach(index => {

        const set = indexes[index]

        const logPrefix = set.node.logPrefix + 'accountNode/close: '

        const path = '/accountNode/close' +
            '?tokens=' + encodeURIComponent(set.tokens.join(','))

        set.node.fetchJson(path, response => {
            if (response === true) return
            app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        }, () => {})

    })

}

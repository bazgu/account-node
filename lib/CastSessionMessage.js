var http = require('http')

var Log = require('./Log.js'),
    ReadText = require('./ReadText.js')

module.exports = (chooseSessionNode, tokens,
    message, responseCallback, deliverCallback) => {

    var indexes = Object.create(null)
    for (var i in tokens) {

        var node = chooseSessionNode.fromSessionToken(i)
        if (node === undefined) continue

        var index = node.index
        var set = indexes[index]
        if (set === undefined) {
            set = indexes[index] = {
                node: node,
                tokens: [],
            }
        }
        set.tokens.push(i)

    }

    var delivered = false
    Object.keys(indexes).forEach(index => {

        function errorListener (err) {
            Log.error(logPrefix + err.code)
        }

        var set = indexes[index]
        var sessionNode = set.node
        var setTokens = set.tokens

        var host = sessionNode.host,
            port = sessionNode.port

        var logPrefix = 'session-node-client: ' + host + ':' + port + ': accountNode/receiveSessionMessage: '

        var path = '/accountNode/receiveSessionMessage' +
            '?tokens=' + encodeURIComponent(setTokens.join(','))

        var proxyReq = http.request({
            host: host,
            port: port,
            path: path,
            method: 'post',
        }, proxyRes => {

            var statusCode = proxyRes.statusCode
            if (statusCode !== 200) {
                Log.error(logPrefix + 'HTTP status code ' + statusCode)
                return
            }

            ReadText(proxyRes, responseText => {

                var invalidTokens = JSON.parse(responseText)
                responseCallback(invalidTokens)

                if (delivered || deliverCallback === undefined ||
                    setTokens.length === invalidTokens.length) return

                delivered = true
                deliverCallback()

            })

        })
        proxyReq.end(JSON.stringify(message))
        proxyReq.on('error', errorListener)

    })

}

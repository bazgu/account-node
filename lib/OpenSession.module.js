module.exports = app => (chooseSessionNode, group,
    username, user, request, longTerm, callback) => {

    const sessionNode = chooseSessionNode.random()

    let path = '/accountNode/open?username=' + encodeURIComponent(username)
    if (longTerm) path += '&longTerm=true'
    path += '&prefix=' + sessionNode.index

    sessionNode.fetchJson(path, token => {
        user.addSession(token, app.Session(sessionNode, token, group, longTerm, Date.now() + 1000 * 60, () => {
            user.removeSession(token)
        }, () => {
            if (!longTerm) return
            user.addLongSession(token, app.LongSession(token, group, Date.now(), () => {
                user.removeLongSession(token)
            }))
        }))
        callback({
            token: token,
            profile: user.getProfile(),
            contacts: user.getContacts(),
            requests: user.getRequests(),
            files: user.getFiles(),
            messages: user.pullMessages(),
        })
    }, () => {
        app.Error500Page(request)
    })

}

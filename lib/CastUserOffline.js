var Log = require('./Log.js')

module.exports = (chooseAccountNode, contacts, username) => {

    var nodes = []
    for (var i in contacts) {
        var accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }

    nodes.forEach(fetchJson => {

        var path = '/accountNode/userOffline' +
            '?username=' + encodeURIComponent(username)

        fetchJson(path, response => {
            if (response === true) return
            Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        })

    })

}

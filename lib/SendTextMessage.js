var Log = require('./Log.js')

module.exports = (chooseAccountNode, username,
    contactUsername, text, time, token) => {

    var path = '/accountNode/receiveTextMessage' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&text=' + encodeURIComponent(text) + '&time=' + time +
        '&token=' + encodeURIComponent(token)

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

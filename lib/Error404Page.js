module.exports = request => {
    request.res.statusCode = 404
    request.respond('NOT_FOUND')
}

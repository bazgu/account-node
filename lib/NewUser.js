var crypto = require('crypto')

var Password = require('./Password.js'),
    User = require('./User.js')

module.exports = (chooseAccountNode,
    chooseSessionNode, username, password, timezone) => {

    var profile = {
        fullName: '',
        fullNamePrivacy: 'contacts',
        email: '',
        emailPrivacy: 'private',
        phone: '',
        phonePrivacy: 'private',
        timezone: timezone,
        timezonePrivacy: 'private',
    }

    var passwordKey = crypto.randomBytes(64)
    var hmac = crypto.createHmac('sha512', passwordKey)
    var passwordDigest = hmac.update(password).digest()

    return User(chooseAccountNode, chooseSessionNode,
        username, Password(passwordKey, passwordDigest),
        profile, Date.now(), Object.create(null), Object.create(null),
        Object.create(null), Object.create(null),
        Object.create(null), [], Object.create(null),
        Object.create(null), Object.create(null))

}

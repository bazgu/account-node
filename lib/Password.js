const crypto = require('crypto')

module.exports = (key, digest) => {
    return {
        change: newPassword => {
            const hmac = crypto.createHmac('sha512', key)
            digest = hmac.update(newPassword).digest()
        },
        matches: otherPassword => {
            if (otherPassword === undefined) return false
            const hmac = crypto.createHmac('sha512', key)
            return hmac.update(otherPassword).digest().compare(digest) === 0
        },
        toStorageObject: () => {
            return {
                key: key.toString('base64'),
                digest: digest.toString('base64'),
            }
        },
    }
}

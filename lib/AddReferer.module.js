module.exports = app => (chooseAccountNode, contactUsername, username, profile) => {

    // TODO
    const logPrefix = ''

    let path = '/accountNode/addReferer' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&refererUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(profile.fullName) +
        '&email=' + encodeURIComponent(profile.email) +
        '&phone=' + encodeURIComponent(profile.phone)

    const timezone = profile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

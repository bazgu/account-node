const crypto = require('crypto')

module.exports = app => chooseSessionNode => {
    return app.SessionGroup(chooseSessionNode, crypto.randomBytes(20).toString('hex'))
}

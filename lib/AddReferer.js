var Log = require('./Log.js')

module.exports = (chooseAccountNode, contactUsername, username, profile) => {

    var path = '/accountNode/addReferer' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&refererUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(profile.fullName) +
        '&email=' + encodeURIComponent(profile.email) +
        '&phone=' + encodeURIComponent(profile.phone)

    var timezone = profile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

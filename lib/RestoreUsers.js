var fs = require('fs')

var RestoreUser = require('./RestoreUser.js'),
    SessionGroup = require('./SessionGroup.js')

module.exports = (chooseAccountNode, chooseSessionNode,
    users, lowerUsers, contactUsers, requestUsers) => {

    function getGroup (identifier) {
        var group = sessionGroups[identifier]
        if (group === undefined) {
            group = sessionGroups[identifier] = SessionGroup(chooseSessionNode, identifier)
        }
        return group
    }

    var sessionGroups = Object.create(null)

    fs.readdirSync('dump').forEach(username => {

        if (username === '.gitignore') return

        var content = fs.readFileSync('dump/' + username, 'utf8')
        var user = RestoreUser(chooseAccountNode, chooseSessionNode,
            getGroup, username, JSON.parse(content))
        users[username] = user
        lowerUsers[username.toLowerCase()] = user

        var contacts = user.getContacts()
        for (var i in contacts) contactUsers.put(i, user)

        var requests = user.getRequests()
        for (var i in requests) requestUsers.put(i, user)

    })

}

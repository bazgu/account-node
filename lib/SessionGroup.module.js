module.exports = app => (chooseSessionNode, identifier) => {
    const sessions = Object.create(null)
    return {
        identifier: identifier,
        add: (token, session) => {
            sessions[token] = session
        },
        close: token => {
            const affectedTokens = []
            for (const i in sessions) {
                if (i !== token) affectedTokens.push(i)
                sessions[i].close()
            }
            app.SendSessionClose(chooseSessionNode, affectedTokens)
        },
        remove: token => {
            delete sessions[token]
        },
    }
}

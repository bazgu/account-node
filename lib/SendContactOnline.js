var Log = require('./Log.js')

module.exports = (chooseAccountNode, username, contactUsername) => {

    var path = '/accountNode/contactOnline' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username)

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

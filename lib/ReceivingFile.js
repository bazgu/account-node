var Log = require('./Log.js')

module.exports = (sessionNode, token, nextCheckTime, destroyListener) => {

    function check () {

        var timeout = setTimeout(() => {

            var path = '/accountNode/checkFile?token=' + encodeURIComponent(token)

            destroyFunction = sessionNode.fetchJson(path, response => {

                if (response === 'INVALID_TOKEN') {
                    destroyListener()
                    return
                }

                if (response !== true) {
                    Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
                    nextCheckTime = Date.now() + 1000 * 10
                    check()
                    return
                }

                nextCheckTime = Date.now() + 1000 * 60
                check()

            }, () => {
                nextCheckTime = Date.now() + 1000 * 10
                check()
            })

        }, nextCheckTime - Date.now())

        destroyFunction = () => {
            clearTimeout(timeout)
        }

    }

    var destroyFunction
    check()

    return {
        destroy: () => {
            destroyFunction()
            destroyListener()
        },
        toStorageObject: () => {
            return { nextCheckTime: nextCheckTime }
        },
    }

}

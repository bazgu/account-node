var crypto = require('crypto')

var SessionGroup = require('./SessionGroup.js')

module.exports = chooseSessionNode => {
    return SessionGroup(chooseSessionNode, crypto.randomBytes(20).toString('hex'))
}

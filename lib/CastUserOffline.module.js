module.exports = app => (chooseAccountNode, contacts, username) => {

    const nodes = []
    for (const i in contacts) {
        const accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }

    nodes.forEach(fetchJson => {

        // TODO
        let logPrefix = ''

        const path = '/accountNode/userOffline' +
            '?username=' + encodeURIComponent(username)

        fetchJson(path, response => {
            if (response === true) return
            app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        })

    })

}

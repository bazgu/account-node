module.exports = password => {
    return password.length >= 6 && !/^\d+$/.test(password)
}

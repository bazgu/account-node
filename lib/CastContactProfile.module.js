module.exports = app => (chooseAccountNode, contacts,
    referers, username, contactsProfile, publicProfile) => {

    const nodes = []
    for (const i in contacts) {
        const accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }
    for (const i in referers) {
        const accountNode = chooseAccountNode.fromUsername(i)
        if (nodes.indexOf(accountNode) === -1) nodes.push(accountNode)
    }

    nodes.forEach(fetchJson => {

        // TODO
        const logPrefix = ''

        let path = '/accountNode/contactProfile' +
            '?username=' + encodeURIComponent(username) +
            '&contactsFullName=' + encodeURIComponent(contactsProfile.fullName) +
            '&contactsEmail=' + encodeURIComponent(contactsProfile.email) +
            '&contactsPhone=' + encodeURIComponent(contactsProfile.phone)

        const contactsTimezone = contactsProfile.timezone
        if (contactsTimezone !== null) {
            path += '&contactsTimezone=' + contactsTimezone
        }

        path += '&publicFullName=' + encodeURIComponent(publicProfile.fullName) +
            '&publicEmail=' + encodeURIComponent(publicProfile.email) +
            '&publicPhone=' + encodeURIComponent(publicProfile.phone)

        const publicTimezone = publicProfile.timezone
        if (publicTimezone !== null) {
            path += '&publicTimezone=' + publicTimezone
        }

        fetchJson(path, response => {
            if (response === true) return
            app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        }, () => {})

    })

}

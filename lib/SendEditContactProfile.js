var Log = require('./Log.js')

module.exports = (chooseAccountNode, username,
    contactUsername, profile, requestReturn) => {

    var path = '/accountNode/editContactProfile' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(profile.fullName) +
        '&email=' + encodeURIComponent(profile.email) +
        '&phone=' + encodeURIComponent(profile.phone)

    var timezone = profile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    if (requestReturn) path += '&requestReturn=true'

    var fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

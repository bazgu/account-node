const crypto = require('crypto')

module.exports = app => loadedConfig => {

    const accountNodes = loadedConfig.accountNodes.map(accountNode => (
        app.FetchJson(accountNode.host, accountNode.port)
    ))

    return {
        thisNode: accountNodes[app.config.index],
        fromUsername: username => {
            username = username.toLowerCase()
            const digest = crypto.createHash('md5').update(username).digest()
            return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
        },
    }

}

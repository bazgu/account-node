const crypto = require('crypto')

module.exports = app => (chooseAccountNode, chooseSessionNode, username,
    password, profile, registerTime, contacts, referers, requests,
    ignoreds, watchers, messages, sessions, longSessions, files) => {

    function castMessage (method, args, excludeToken, deliverCallback) {

        const tokens = Object.create(null)
        for (const i in sessions) {
            if (i !== excludeToken) tokens[i] = true
        }

        app.CastSessionMessage(chooseSessionNode, tokens, [method, args], invalidTokens => {
            invalidTokens.forEach(token => {
                const session = sessions[token]
                if (session !== undefined) session.expire()
            })
        }, deliverCallback)

    }

    function castMessageStore (method, args) {
        const message = [method, args]
        messages.push(message)
        if (Object.keys(sessions).length === 0) return
        castMessage(method, args, undefined, () => {
            const index = messages.indexOf(message)
            if (index !== -1) messages.splice(index, 1)
        })
    }

    function editRequest (username, profile) {
        requests[username] = profile
        castMessage('editRequest', [username, profile])
    }

    function removeFile (token) {
        delete files[token]
        castMessage('removeFile', [token])
    }

    const contactsProfile = {
        fullName: profile.fullName,
        email: '',
        phone: '',
        timezone: null,
    }

    const publicProfile = {
        fullName: '',
        email: '',
        phone: '',
        timezone: null,
    }

    return {
        changePassword: password.change,
        editRequest: editRequest,
        passwordMatches: password.matches,
        removeFile: removeFile,
        username: username,
        addContact: (contactUsername, contact, token) => {
            let requestRemoved = false
            contacts[contactUsername] = contact
            castMessage('addContact', [contactUsername, contact], token)
            if (requests[contactUsername] !== undefined) {
                delete requests[contactUsername]
                requestRemoved = true
            }
            if (ignoreds[contactUsername] !== undefined) {
                delete ignoreds[contactUsername]
            }
            app.AddReferer(chooseAccountNode, contactUsername, username, contactsProfile)
            return requestRemoved
        },
        addLongSession: (token, session) => {
            longSessions[token] = session
        },
        addReferer: (refererUsername, profile) => {

            let requestAdded = false
            referers[refererUsername] = {}

            const contact = contacts[refererUsername]
            if (contact === undefined) {
                const request = requests[refererUsername]
                if (request === undefined && ignoreds[refererUsername] === undefined) {
                    requests[refererUsername] = profile
                    requestAdded = true
                    castMessage('addRequest', [refererUsername, profile])
                } else if (request !== undefined) {
                    editRequest(refererUsername, profile)
                }
            } else {
                contact.online = true
                contact.profile = profile
                castMessage('editContactProfileAndOnline', [refererUsername, profile])
                if (Object.keys(sessions).length !== 0) {
                    app.SendContactOnline(chooseAccountNode, username, refererUsername)
                }
                app.SendEditContactProfile(chooseAccountNode, username, refererUsername, contactsProfile, true)
            }

            return requestAdded

        },
        addSession: (token, session) => {

            sessions[token] = session

            if (Object.keys(sessions).length === 1) {
                app.CastUserOnline(chooseAccountNode, contacts, username)
            }

        },
        contactOffline: contactUsername => {
            contacts[contactUsername].online = false
            castMessage('offline', [contactUsername])
        },
        contactOnline: contactUsername => {
            contacts[contactUsername].online = true
            castMessage('online', [contactUsername])
        },
        editContactProfile: (contactUsername, contactProfile, requestReturn) => {
            contacts[contactUsername].profile = contactProfile
            castMessage('editContactProfile', [contactUsername, contactProfile])
            if (requestReturn) {
                app.SendEditContactProfile(chooseAccountNode, username, contactUsername, contactsProfile, false)
            }
        },
        editProfile: (newProfile, token) => {

            let profileChanged = false,
                contactsProfileChanged = false,
                publicProfileChanged = false

            const fullName = newProfile.fullName
            if (profile.fullName !== fullName) {
                profile.fullName = fullName
                profileChanged = true
            }

            const fullNamePrivacy = newProfile.fullNamePrivacy
            if (profile.fullNamePrivacy !== fullNamePrivacy) {
                profile.fullNamePrivacy = fullNamePrivacy
                profileChanged = true
            }

            const contactsFullName = fullNamePrivacy === 'private' ? '' : fullName
            if (contactsProfile.fullName !== contactsFullName) {
                contactsProfile.fullName = contactsFullName
                contactsProfileChanged = true
            }

            const publicFullName = fullNamePrivacy === 'public' ? fullName : ''
            if (publicProfile.fullName !== publicFullName) {
                publicProfile.fullName = publicFullName
                publicProfileChanged = true
            }

            const email = newProfile.email
            if (profile.email !== email) {
                profile.email = email
                profileChanged = true
            }

            const emailPrivacy = newProfile.emailPrivacy
            if (profile.emailPrivacy !== emailPrivacy) {
                profile.emailPrivacy = emailPrivacy
                profileChanged = true
            }

            const contactsEmail = emailPrivacy === 'private' ? '' : email
            if (contactsProfile.email !== contactsEmail) {
                contactsProfile.email = contactsEmail
                contactsProfileChanged = true
            }

            const publicEmail = emailPrivacy === 'public' ? email : ''
            if (publicProfile.email !== publicEmail) {
                publicProfile.email = publicEmail
                publicProfileChanged = true
            }

            const phone = newProfile.phone
            if (profile.phone !== phone) {
                profile.phone = phone
                profileChanged = true
            }

            const phonePrivacy = newProfile.phonePrivacy
            if (profile.phonePrivacy !== phonePrivacy) {
                profile.phonePrivacy = phonePrivacy
                profileChanged = true
            }

            const contactsPhone = phonePrivacy === 'private' ? '' : phone
            if (contactsProfile.phone !== contactsPhone) {
                contactsProfile.phone = contactsPhone
                contactsProfileChanged = true
            }

            const publicPhone = phonePrivacy === 'public' ? phone : ''
            if (publicProfile.phone !== publicPhone) {
                publicProfile.phone = publicPhone
                publicProfileChanged = true
            }

            const timezone = newProfile.timezone
            if (profile.timezone !== timezone) {
                profile.timezone = timezone
                profileChanged = true
            }

            const timezonePrivacy = newProfile.timezonePrivacy
            if (profile.timezonePrivacy !== timezonePrivacy) {
                profile.timezonePrivacy = timezonePrivacy
                profileChanged = true
            }

            const contactsTimezone = timezonePrivacy === 'private' ? null : timezone
            if (contactsProfile.timezone !== contactsTimezone) {
                contactsProfile.timezone = contactsTimezone
                contactsProfileChanged = true
            }

            const publicTimezone = timezonePrivacy === 'public' ? timezone : null
            if (publicProfile.timezone !== publicTimezone) {
                publicProfile.timezone = publicTimezone
                publicProfileChanged = true
            }

            if (profileChanged) castMessage('editProfile', [profile], token)
            if (contactsProfileChanged || publicProfileChanged) {
                app.CastContactProfile(chooseAccountNode, contacts, referers, username, contactsProfile, publicProfile)
                if (publicProfileChanged) {
                    app.CastSessionMessage(chooseSessionNode, watchers, ['publicProfile', [username, publicProfile]], invalidTokens => {
                        invalidTokens.forEach(token => {
                            delete watchers[token]
                        })
                    })
                }
            }

        },
        getContact: username => {
            return contacts[username]
        },
        getContacts: () => {
            return contacts
        },
        getFile: token => {
            return files[token]
        },
        getFiles: () => {
            return files
        },
        getLongSession: token => {
            return longSessions[token]
        },
        getProfile: () => {
            return profile
        },
        getReferer: username => {
            return referers[username]
        },
        getRequest: username => {
            return requests[username]
        },
        getRequests: () => {
            return requests
        },
        getSession: token => {
            return sessions[token]
        },
        getWatcher: token => {
            return watchers[token]
        },
        ignoreRequest: (requestUsername, token) => {
            delete requests[requestUsername]
            castMessage('ignoreRequest', [requestUsername], token)
            ignoreds[requestUsername] = {}
        },
        overrideContactProfile: (contactUsername, overrideProfile, token) => {
            contacts[contactUsername].overrideProfile = overrideProfile
            castMessage('overrideContactProfile', [contactUsername, overrideProfile], token)
        },
        pullMessages: () => {
            return messages.splice(0)
        },
        receiveFileMessage: (contactUsername, file, time, token) => {

            const fileToken = file.token

            const sessionNode = chooseSessionNode.fromFileToken(fileToken)
            if (sessionNode === undefined) return

            files[fileToken] = app.ReceivingFile(sessionNode, fileToken, Date.now() + 1000 * 60, () => {
                removeFile(fileToken)
            })

            castMessageStore('receiveFileMessage', [contactUsername, file, time, token])

        },
        receiveTextMessage: (contactUsername, text, time, token) => {
            castMessageStore('receiveTextMessage', [contactUsername, text, time, token])
        },
        removeContact: (contactUsername, token) => {
            delete contacts[contactUsername]
            castMessage('removeContact', [contactUsername], token)
            app.RemoveReferer(chooseAccountNode, contactUsername, username, publicProfile)
        },
        removeLongSession: token => {
            delete longSessions[token]
        },
        removeReferer: (refererUsername, publicProfile) => {

            delete referers[refererUsername]

            const contact = contacts[refererUsername]
            if (contact === undefined || !contact.online) return
            contact.online = false
            contact.profile = publicProfile
            castMessage('editContactProfileAndOffline', [refererUsername, publicProfile])

        },
        removeRequest: (requestUsername, token) => {
            delete requests[requestUsername]
            castMessage('removeRequest', [requestUsername], token)
        },
        removeSession: token => {

            delete sessions[token]

            if (Object.keys(sessions).length === 0) {
                app.CastUserOffline(chooseAccountNode, contacts, username)
            }

        },
        sendFileMessage: (contactUsername, file, token) => {

            const time = Date.now(),
                messageToken = crypto.randomBytes(20).toString('hex')

            castMessage('sendFileMessage', [contactUsername, file, time], token)
            app.SendFileMessage(chooseAccountNode, username, contactUsername, file, time, messageToken)

            return {
                time: time,
                token: messageToken,
            }

        },
        sendTextMessage: (contactUsername, text, token) => {

            const time = Date.now(),
                messageToken = crypto.randomBytes(20).toString('hex')

            castMessage('sendTextMessage', [contactUsername, text, time], token)
            app.SendTextMessage(chooseAccountNode, username, contactUsername, text, time, messageToken)

            return {
                time: time,
                token: messageToken,
            }

        },
        toStorageObject: () => {
            return {
                password: password.toStorageObject(),
                profile: profile,
                registerTime: registerTime,
                contacts: contacts,
                referers: referers,
                requests: requests,
                ignoreds: ignoreds,
                watchers: watchers,
                messages: messages,
                sessions: (() => {
                    const object = {}
                    for (const i in sessions) {
                        object[i] = sessions[i].toStorageObject()
                    }
                    return object
                })(),
                longSessions: (() => {
                    const object = {}
                    for (const i in longSessions) {
                        object[i] = longSessions[i].toStorageObject()
                    }
                    return object
                })(),
                files: (() => {
                    const object = {}
                    for (const i in files) {
                        object[i] = files[i].toStorageObject()
                    }
                    return object
                })(),
            }

        },
        unwatchPublicProfile: token => {
            delete watchers[token]
        },
        watchPublicProfile: token => {
            watchers[token] = {}
            return {
                username: username,
                profile: publicProfile,
            }
        },
    }

}

module.exports = app => () => {

    app.LoadConfig(loadedConfig => {

        function dump () {
            for (const i in users) {
                const object = users[i].toStorageObject()
                fs.writeFileSync('dump/' + i, JSON.stringify(object, null, 4))
            }
        }

        function shutdown () {
            dump()
            process.exit()
        }

        const fs = require('fs'),
            url = require('url')

        const chooseAccountNode = app.ChooseAccountNode(loadedConfig),
            chooseCaptchaNode = app.ChooseCaptchaNode(loadedConfig),
            chooseSessionNode = app.ChooseSessionNode(loadedConfig)

        const contactUsers = app.RefUsers(),
            requestUsers = app.RefUsers()

        const users = Object.create(null),
            lowerUsers = Object.create(null)

        app.RestoreUsers(chooseAccountNode, chooseSessionNode,
            users, lowerUsers, contactUsers, requestUsers)

        const pages = Object.create(null)
        pages['/'] = app.Page.Index
        pages['/accountNode/addReferer'] = app.Page.AccountNode.AddReferer(users, requestUsers)
        pages['/accountNode/contactOnline'] = app.Page.AccountNode.ContactOnline(users)
        pages['/accountNode/contactProfile'] = app.Page.AccountNode.ContactProfile(contactUsers, requestUsers)
        pages['/accountNode/editContactProfile'] = app.Page.AccountNode.EditContactProfile(users)
        pages['/accountNode/receiveFileMessage'] = app.Page.AccountNode.ReceiveFileMessage(users)
        pages['/accountNode/receiveTextMessage'] = app.Page.AccountNode.ReceiveTextMessage(users)
        pages['/accountNode/removeReferer'] = app.Page.AccountNode.RemoveReferer(users)
        pages['/accountNode/userOffline'] = app.Page.AccountNode.UserOffline(contactUsers)
        pages['/accountNode/userOnline'] = app.Page.AccountNode.UserOnline(contactUsers)
        pages['/frontNode/restoreSession'] = app.Page.FrontNode.RestoreSession(chooseSessionNode, users)
        pages['/frontNode/signIn'] = app.Page.FrontNode.SignIn(chooseSessionNode, lowerUsers)
        pages['/frontNode/signUp'] = app.Page.FrontNode.SignUp(chooseAccountNode, chooseCaptchaNode, chooseSessionNode, users, lowerUsers)
        pages['/sessionNode/addContact'] = app.Page.SessionNode.AddContact(users, contactUsers, requestUsers)
        pages['/sessionNode/changePassword'] = app.Page.SessionNode.ChangePassword(users)
        pages['/sessionNode/editProfile'] = app.Page.SessionNode.EditProfile(users)
        pages['/sessionNode/expireSession'] = app.Page.SessionNode.ExpireSession(users)
        pages['/sessionNode/ignoreRequest'] = app.Page.SessionNode.IgnoreRequest(users, requestUsers)
        pages['/sessionNode/overrideContactProfile'] = app.Page.SessionNode.OverrideContactProfile(users)
        pages['/sessionNode/removeContact'] = app.Page.SessionNode.RemoveContact(users, contactUsers)
        pages['/sessionNode/removeFile'] = app.Page.SessionNode.RemoveFile(users)
        pages['/sessionNode/removeRequest'] = app.Page.SessionNode.RemoveRequest(users, requestUsers)
        pages['/sessionNode/sendFileMessage'] = app.Page.SessionNode.SendFileMessage(users)
        pages['/sessionNode/sendTextMessage'] = app.Page.SessionNode.SendTextMessage(users)
        pages['/sessionNode/signOut'] = app.Page.SessionNode.SignOut(users)
        pages['/sessionNode/unwatchPublicProfile'] = app.Page.SessionNode.UnwatchPublicProfile(users)
        pages['/sessionNode/watchPublicProfile'] = app.Page.SessionNode.WatchPublicProfile(lowerUsers)
        app.Static(pages)

        require('http').createServer((req, res) => {

            app.log.info('Http ' + req.method + ' ' + req.url)

            const parsed_url = url.parse(req.url, true)
            const request = {
                parsed_url, req, res,
                respond (response) {
                    app.EchoText(request, {
                        type: 'application/json',
                        content: JSON.stringify(response),
                    })
                },
            }

            const page = (() => {
                const page = pages[parsed_url.pathname]
                if (page !== undefined) return page
                return app.Error404Page
            })()

            page(request)

        }).listen(listen.port, listen.host)

        process.on('SIGINT', shutdown)
        process.on('SIGTERM', shutdown)

        setInterval(dump, 1000 * 60 * 10)

    })

    app.Watch(['lib', 'static'])

    const listen = app.config.listen

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}

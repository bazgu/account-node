var crypto = require('crypto')

var FetchJson = require('./FetchJson.js')

var config = require('../config.js')

module.exports = loadedConfig => {

    var accountNodes = loadedConfig.accountNodes.map(accountNode => {
        return FetchJson(accountNode.host, accountNode.port)
    })

    return {
        thisNode: accountNodes[config.index],
        fromUsername: username => {
            username = username.toLowerCase()
            var digest = crypto.createHash('md5').update(username).digest()
            return accountNodes[digest.readUInt32LE(0) % accountNodes.length]
        },
    }

}

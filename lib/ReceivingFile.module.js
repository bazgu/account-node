module.exports = app => (sessionNode, token, nextCheckTime, destroyListener) => {

    function check () {

        const timeout = setTimeout(() => {

            const logPrefix = sessionNode.logPrefix + 'accountNode/checkFile: '

            const path = '/accountNode/checkFile?token=' + encodeURIComponent(token)

            destroyFunction = sessionNode.fetchJson(path, response => {

                if (response === 'INVALID_TOKEN') {
                    destroyListener()
                    return
                }

                if (response !== true) {
                    app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
                    nextCheckTime = Date.now() + 1000 * 10
                    check()
                    return
                }

                nextCheckTime = Date.now() + 1000 * 60
                check()

            }, () => {
                nextCheckTime = Date.now() + 1000 * 10
                check()
            })

        }, nextCheckTime - Date.now())

        destroyFunction = () => {
            clearTimeout(timeout)
        }

    }

    let destroyFunction
    check()

    return {
        destroy: () => {
            destroyFunction()
            destroyListener()
        },
        toStorageObject: () => {
            return { nextCheckTime: nextCheckTime }
        },
    }

}

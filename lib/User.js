var crypto = require('crypto')

var AddReferer = require('./AddReferer.js'),
    CastContactProfile = require('./CastContactProfile.js'),
    CastSessionMessage = require('./CastSessionMessage.js'),
    CastUserOnline = require('./CastUserOnline.js'),
    CastUserOffline = require('./CastUserOffline.js'),
    ReceivingFile = require('./ReceivingFile.js'),
    RemoveReferer = require('./RemoveReferer.js'),
    SendContactOnline = require('./SendContactOnline.js'),
    SendEditContactProfile = require('./SendEditContactProfile.js'),
    SendFileMessage = require('./SendFileMessage.js'),
    SendTextMessage = require('./SendTextMessage.js')

module.exports = (chooseAccountNode, chooseSessionNode, username,
    password, profile, registerTime, contacts, referers, requests,
    ignoreds, watchers, messages, sessions, longSessions, files) => {

    function castMessage (method, args, excludeToken, deliverCallback) {

        var tokens = Object.create(null)
        for (var i in sessions) {
            if (i !== excludeToken) tokens[i] = true
        }

        CastSessionMessage(chooseSessionNode, tokens, [method, args], invalidTokens => {
            invalidTokens.forEach(token => {
                var session = sessions[token]
                if (session !== undefined) session.expire()
            })
        }, deliverCallback)

    }

    function castMessageStore (method, args) {
        var message = [method, args]
        messages.push(message)
        if (Object.keys(sessions).length === 0) return
        castMessage(method, args, undefined, () => {
            var index = messages.indexOf(message)
            if (index !== -1) messages.splice(index, 1)
        })
    }

    function editRequest (username, profile) {
        requests[username] = profile
        castMessage('editRequest', [username, profile])
    }

    function removeFile (token) {
        delete files[token]
        castMessage('removeFile', [token])
    }

    var contactsProfile = {
        fullName: profile.fullName,
        email: '',
        phone: '',
        timezone: null,
    }

    var publicProfile = {
        fullName: '',
        email: '',
        phone: '',
        timezone: null,
    }

    return {
        changePassword: password.change,
        editRequest: editRequest,
        passwordMatches: password.matches,
        removeFile: removeFile,
        username: username,
        addContact: (contactUsername, contact, token) => {
            var requestRemoved = false
            contacts[contactUsername] = contact
            castMessage('addContact', [contactUsername, contact], token)
            if (requests[contactUsername] !== undefined) {
                delete requests[contactUsername]
                requestRemoved = true
            }
            if (ignoreds[contactUsername] !== undefined) {
                delete ignoreds[contactUsername]
            }
            AddReferer(chooseAccountNode, contactUsername, username, contactsProfile)
            return requestRemoved
        },
        addLongSession: (token, session) => {
            longSessions[token] = session
        },
        addReferer: (refererUsername, profile) => {

            var requestAdded = false
            referers[refererUsername] = {}

            var contact = contacts[refererUsername]
            if (contact === undefined) {
                var request = requests[refererUsername]
                if (request === undefined && ignoreds[refererUsername] === undefined) {
                    requests[refererUsername] = profile
                    requestAdded = true
                    castMessage('addRequest', [refererUsername, profile])
                } else if (request !== undefined) {
                    editRequest(refererUsername, profile)
                }
            } else {
                contact.online = true
                contact.profile = profile
                castMessage('editContactProfileAndOnline', [refererUsername, profile])
                if (Object.keys(sessions).length !== 0) {
                    SendContactOnline(chooseAccountNode, username, refererUsername)
                }
                SendEditContactProfile(chooseAccountNode, username, refererUsername, contactsProfile, true)
            }

            return requestAdded

        },
        addSession: (token, session) => {

            sessions[token] = session

            if (Object.keys(sessions).length === 1) {
                CastUserOnline(chooseAccountNode, contacts, username)
            }

        },
        contactOffline: contactUsername => {
            contacts[contactUsername].online = false
            castMessage('offline', [contactUsername])
        },
        contactOnline: contactUsername => {
            contacts[contactUsername].online = true
            castMessage('online', [contactUsername])
        },
        editContactProfile: (contactUsername, contactProfile, requestReturn) => {
            contacts[contactUsername].profile = contactProfile
            castMessage('editContactProfile', [contactUsername, contactProfile])
            if (requestReturn) {
                SendEditContactProfile(chooseAccountNode, username, contactUsername, contactsProfile, false)
            }
        },
        editProfile: (newProfile, token) => {

            var profileChanged = false,
                contactsProfileChanged = false,
                publicProfileChanged = false

            var fullName = newProfile.fullName
            if (profile.fullName !== fullName) {
                profile.fullName = fullName
                profileChanged = true
            }

            var fullNamePrivacy = newProfile.fullNamePrivacy
            if (profile.fullNamePrivacy !== fullNamePrivacy) {
                profile.fullNamePrivacy = fullNamePrivacy
                profileChanged = true
            }

            var contactsFullName = fullNamePrivacy === 'private' ? '' : fullName
            if (contactsProfile.fullName !== contactsFullName) {
                contactsProfile.fullName = contactsFullName
                contactsProfileChanged = true
            }

            var publicFullName = fullNamePrivacy === 'public' ? fullName : ''
            if (publicProfile.fullName !== publicFullName) {
                publicProfile.fullName = publicFullName
                publicProfileChanged = true
            }

            var email = newProfile.email
            if (profile.email !== email) {
                profile.email = email
                profileChanged = true
            }

            var emailPrivacy = newProfile.emailPrivacy
            if (profile.emailPrivacy !== emailPrivacy) {
                profile.emailPrivacy = emailPrivacy
                profileChanged = true
            }

            var contactsEmail = emailPrivacy === 'private' ? '' : email
            if (contactsProfile.email !== contactsEmail) {
                contactsProfile.email = contactsEmail
                contactsProfileChanged = true
            }

            var publicEmail = emailPrivacy === 'public' ? email : ''
            if (publicProfile.email !== publicEmail) {
                publicProfile.email = publicEmail
                publicProfileChanged = true
            }

            var phone = newProfile.phone
            if (profile.phone !== phone) {
                profile.phone = phone
                profileChanged = true
            }

            var phonePrivacy = newProfile.phonePrivacy
            if (profile.phonePrivacy !== phonePrivacy) {
                profile.phonePrivacy = phonePrivacy
                profileChanged = true
            }

            var contactsPhone = phonePrivacy === 'private' ? '' : phone
            if (contactsProfile.phone !== contactsPhone) {
                contactsProfile.phone = contactsPhone
                contactsProfileChanged = true
            }

            var publicPhone = phonePrivacy === 'public' ? phone : ''
            if (publicProfile.phone !== publicPhone) {
                publicProfile.phone = publicPhone
                publicProfileChanged = true
            }

            var timezone = newProfile.timezone
            if (profile.timezone !== timezone) {
                profile.timezone = timezone
                profileChanged = true
            }

            var timezonePrivacy = newProfile.timezonePrivacy
            if (profile.timezonePrivacy !== timezonePrivacy) {
                profile.timezonePrivacy = timezonePrivacy
                profileChanged = true
            }

            var contactsTimezone = timezonePrivacy === 'private' ? null : timezone
            if (contactsProfile.timezone !== contactsTimezone) {
                contactsProfile.timezone = contactsTimezone
                contactsProfileChanged = true
            }

            var publicTimezone = timezonePrivacy === 'public' ? timezone : null
            if (publicProfile.timezone !== publicTimezone) {
                publicProfile.timezone = publicTimezone
                publicProfileChanged = true
            }

            if (profileChanged) castMessage('editProfile', [profile], token)
            if (contactsProfileChanged || publicProfileChanged) {
                CastContactProfile(chooseAccountNode, contacts, referers, username, contactsProfile, publicProfile)
                if (publicProfileChanged) {
                    CastSessionMessage(chooseSessionNode, watchers, ['publicProfile', [username, publicProfile]], invalidTokens => {
                        invalidTokens.forEach(token => {
                            delete watchers[token]
                        })
                    })
                }
            }

        },
        getContact: username => {
            return contacts[username]
        },
        getContacts: () => {
            return contacts
        },
        getFile: token => {
            return files[token]
        },
        getFiles: () => {
            return files
        },
        getLongSession: token => {
            return longSessions[token]
        },
        getProfile: () => {
            return profile
        },
        getReferer: username => {
            return referers[username]
        },
        getRequest: username => {
            return requests[username]
        },
        getRequests: () => {
            return requests
        },
        getSession: token => {
            return sessions[token]
        },
        getWatcher: token => {
            return watchers[token]
        },
        ignoreRequest: (requestUsername, token) => {
            delete requests[requestUsername]
            castMessage('ignoreRequest', [requestUsername], token)
            ignoreds[requestUsername] = {}
        },
        overrideContactProfile: (contactUsername, overrideProfile, token) => {
            contacts[contactUsername].overrideProfile = overrideProfile
            castMessage('overrideContactProfile', [contactUsername, overrideProfile], token)
        },
        pullMessages: () => {
            return messages.splice(0)
        },
        receiveFileMessage: (contactUsername, file, time, token) => {

            var fileToken = file.token

            var sessionNode = chooseSessionNode.fromFileToken(fileToken)
            if (sessionNode === undefined) return

            files[fileToken] = ReceivingFile(sessionNode, fileToken, Date.now() + 1000 * 60, () => {
                removeFile(fileToken)
            })

            castMessageStore('receiveFileMessage', [contactUsername, file, time, token])

        },
        receiveTextMessage: (contactUsername, text, time, token) => {
            castMessageStore('receiveTextMessage', [contactUsername, text, time, token])
        },
        removeContact: (contactUsername, token) => {
            delete contacts[contactUsername]
            castMessage('removeContact', [contactUsername], token)
            RemoveReferer(chooseAccountNode, contactUsername, username, publicProfile)
        },
        removeLongSession: token => {
            delete longSessions[token]
        },
        removeReferer: (refererUsername, publicProfile) => {

            delete referers[refererUsername]

            var contact = contacts[refererUsername]
            if (contact === undefined || !contact.online) return
            contact.online = false
            contact.profile = publicProfile
            castMessage('editContactProfileAndOffline', [refererUsername, publicProfile])

        },
        removeRequest: (requestUsername, token) => {
            delete requests[requestUsername]
            castMessage('removeRequest', [requestUsername], token)
        },
        removeSession: token => {

            delete sessions[token]

            if (Object.keys(sessions).length === 0) {
                CastUserOffline(chooseAccountNode, contacts, username)
            }

        },
        sendFileMessage: (contactUsername, file, token) => {

            var time = Date.now(),
                messageToken = crypto.randomBytes(20).toString('hex')

            castMessage('sendFileMessage', [contactUsername, file, time], token)
            SendFileMessage(chooseAccountNode, username, contactUsername, file, time, messageToken)

            return {
                time: time,
                token: messageToken,
            }

        },
        sendTextMessage: (contactUsername, text, token) => {

            var time = Date.now(),
                messageToken = crypto.randomBytes(20).toString('hex')

            var messageToken = crypto.randomBytes(20).toString('hex')
            castMessage('sendTextMessage', [contactUsername, text, time], token)
            SendTextMessage(chooseAccountNode, username, contactUsername, text, time, messageToken)

            return {
                time: time,
                token: messageToken,
            }

        },
        toStorageObject: () => {
            return {
                password: password.toStorageObject(),
                profile: profile,
                registerTime: registerTime,
                contacts: contacts,
                referers: referers,
                requests: requests,
                ignoreds: ignoreds,
                watchers: watchers,
                messages: messages,
                sessions: (() => {
                    var object = {}
                    for (var i in sessions) {
                        object[i] = sessions[i].toStorageObject()
                    }
                    return object
                })(),
                longSessions: (() => {
                    var object = {}
                    for (var i in longSessions) {
                        object[i] = longSessions[i].toStorageObject()
                    }
                    return object
                })(),
                files: (() => {
                    var object = {}
                    for (var i in files) {
                        object[i] = files[i].toStorageObject()
                    }
                    return object
                })(),
            }

        },
        unwatchPublicProfile: token => {
            delete watchers[token]
        },
        watchPublicProfile: token => {
            watchers[token] = {}
            return {
                username: username,
                profile: publicProfile,
            }
        },
    }

}

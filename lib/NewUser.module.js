const crypto = require('crypto')

module.exports = app => (chooseAccountNode,
    chooseSessionNode, username, password, timezone) => {

    const profile = {
        fullName: '',
        fullNamePrivacy: 'contacts',
        email: '',
        emailPrivacy: 'private',
        phone: '',
        phonePrivacy: 'private',
        timezone: timezone,
        timezonePrivacy: 'private',
    }

    const passwordKey = crypto.randomBytes(64)
    const hmac = crypto.createHmac('sha512', passwordKey)
    const passwordDigest = hmac.update(password).digest()

    return app.User(chooseAccountNode, chooseSessionNode,
        username, app.Password(passwordKey, passwordDigest),
        profile, Date.now(), Object.create(null), Object.create(null),
        Object.create(null), Object.create(null),
        Object.create(null), [], Object.create(null),
        Object.create(null), Object.create(null))

}

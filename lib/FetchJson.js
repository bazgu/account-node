var http = require('http')

var Log = require('./Log.js'),
    ReadText = require('./ReadText.js')

module.exports = (host, port) => {
    return (path, doneCallback, errorCallback) => {

        function errorListener (err) {
            Log.error(logPrefix + err.code)
            errorCallback()
        }

        var logPrefix = 'fetchJson http://' + host + ':' + port + path + ' '

        Log.info(logPrefix)
        var req = http.get({
            host: host,
            port: port,
            path: path,
        }, res => {

            var statusCode = res.statusCode
            if (statusCode !== 200) {
                Log.error(logPrefix + 'HTTP status code ' + statusCode)
                req.abort()
                errorCallback()
                return
            }

            ReadText(res, responseText => {

                try {
                    var response = JSON.parse(responseText)
                } catch (e) {
                    Log.error(logPrefix + 'Invalid JSON document: ' + JSON.stringify(responseText))
                    errorCallback()
                    return
                }

                doneCallback(response)

            })

        })
        req.on('error', errorListener)

        return () => {
            req.abort()
        }

    }
}

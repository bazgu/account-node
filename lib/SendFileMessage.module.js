module.exports = app => (chooseAccountNode, username,
    contactUsername, file, time, token) => {

    // TODO
    const logPrefix = ''

    const path = '/accountNode/receiveFileMessage' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&fileToken=' + encodeURIComponent(file.token) +
        '&name=' + encodeURIComponent(file.name) +
        '&size=' + encodeURIComponent(file.size) + '&time=' + time +
        '&token=' + encodeURIComponent(token)

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

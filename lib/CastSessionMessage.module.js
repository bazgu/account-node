const http = require('http')

module.exports = app => (chooseSessionNode, tokens,
    message, responseCallback, deliverCallback) => {

    const indexes = Object.create(null)
    for (const i in tokens) {

        const node = chooseSessionNode.fromSessionToken(i)
        if (node === undefined) continue

        const index = node.index
        let set = indexes[index]
        if (set === undefined) {
            set = indexes[index] = {
                node: node,
                tokens: [],
            }
        }
        set.tokens.push(i)

    }

    let delivered = false
    Object.keys(indexes).forEach(index => {

        function errorListener (err) {
            app.log.error(logPrefix + err.code)
        }

        const set = indexes[index]
        const sessionNode = set.node
        const setTokens = set.tokens

        const host = sessionNode.host,
            port = sessionNode.port

        const logPrefix = 'session-node: ' + host + ':' + port + ': accountNode/receiveSessionMessage: '

        const path = '/accountNode/receiveSessionMessage' +
            '?tokens=' + encodeURIComponent(setTokens.join(','))

        const proxyReq = http.request({
            host: host,
            port: port,
            path: path,
            method: 'post',
        }, proxyRes => {

            const statusCode = proxyRes.statusCode
            if (statusCode !== 200) {
                app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                return
            }

            app.ReadText(proxyRes, responseText => {

                const invalidTokens = JSON.parse(responseText)
                responseCallback(invalidTokens)

                if (delivered || deliverCallback === undefined ||
                    setTokens.length === invalidTokens.length) return

                delivered = true
                deliverCallback()

            })

        })
        proxyReq.end(JSON.stringify(message))
        proxyReq.on('error', errorListener)

    })

}

module.exports = request => {
    request.res.statusCode = 500
    request.respond('INTERNAL_SERVER_ERROR')
}

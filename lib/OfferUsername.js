const crypto = require('crypto')

module.exports = (chooseAccountNode, unavailableUsername, lowerUsers) => {

    function trySuffix (suffix) {

        const username = unavailableUsername + suffix
        if (lowerUsers[username.toLowerCase()] !== undefined) return

        const accountNode = chooseAccountNode.fromUsername(username)
        if (accountNode === chooseAccountNode.thisNode) return username

    }

    let length = 1
    const endsWithDigit = unavailableUsername.match(/\d$/)
    while (true) {
        if (endsWithDigit) {
            for (let i = 0; i < length * 5; i++) {

                let randomString = crypto.randomBytes(length * 5)
                randomString = randomString.toString('base64').toLowerCase()
                randomString = randomString.replace(/[^a-z]/g, '').substr(0, length)

                const username = trySuffix(randomString)
                if (username !== undefined) return username

            }
        } else {
            for (let i = 0; i < length * 5; i++) {

                let randomString = crypto.randomBytes(length * 5).toString('hex')
                randomString = randomString.replace(/[^0-9]/g, '').substr(0, length)

                const username = trySuffix(randomString)
                if (username !== undefined) return username

            }
        }
        for (let i = 0; i < length * 5; i++) {

            let randomString = crypto.randomBytes(length * 5).toString('base64').toLowerCase()
            randomString = randomString.replace(/[^a-z0-9]/g, '').substr(0, length)

            const username1 = trySuffix('.' + randomString)
            if (username1 !== undefined) return username1

            const username2 = trySuffix('-' + randomString)
            if (username2 !== undefined) return username2

            const username3 = trySuffix('_' + randomString)
            if (username3 !== undefined) return username3

        }
        length++
    }

}

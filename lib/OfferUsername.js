var crypto = require('crypto')

module.exports = (chooseAccountNode, unavailableUsername, lowerUsers) => {

    function trySuffix (suffix) {

        var username = unavailableUsername + suffix
        if (lowerUsers[username.toLowerCase()] !== undefined) return

        var accountNode = chooseAccountNode.fromUsername(username)
        if (accountNode === chooseAccountNode.thisNode) return username

    }

    var length = 1
    var endsWithDigit = unavailableUsername.match(/\d$/)
    while (true) {
        if (endsWithDigit) {
            for (var i = 0; i < length * 5; i++) {

                var randomString = crypto.randomBytes(length * 5)
                randomString = randomString.toString('base64').toLowerCase()
                randomString = randomString.replace(/[^a-z]/g, '').substr(0, length)

                var username = trySuffix(randomString)
                if (username !== undefined) return username

            }
        } else {
            for (var i = 0; i < length * 5; i++) {

                var randomString = crypto.randomBytes(length * 5).toString('hex')
                randomString = randomString.replace(/[^0-9]/g, '').substr(0, length)

                var username = trySuffix(randomString)
                if (username !== undefined) return username

            }
        }
        for (var i = 0; i < length * 5; i++) {

            var randomString = crypto.randomBytes(length * 5).toString('base64').toLowerCase()
            randomString = randomString.replace(/[^a-z0-9]/g, '').substr(0, length)

            var username = trySuffix('.' + randomString)
            if (username !== undefined) return username

            var username = trySuffix('-' + randomString)
            if (username !== undefined) return username

            var username = trySuffix('_' + randomString)
            if (username !== undefined) return username

        }
        length++
    }

}

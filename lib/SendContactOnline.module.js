module.exports = app => (chooseAccountNode, username, contactUsername) => {

    // TODO
    const logPrefix = ''

    const path = '/accountNode/contactOnline' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username)

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

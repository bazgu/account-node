module.exports = app => (chooseAccountNode, username,
    contactUsername, text, time, token) => {

    // TODO
    const logPrefix = ''

    const path = '/accountNode/receiveTextMessage' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&text=' + encodeURIComponent(text) + '&time=' + time +
        '&token=' + encodeURIComponent(token)

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

module.exports = app => (chooseAccountNode, username,
    contactUsername, profile, requestReturn) => {

    // TODO
    const logPrefix = ''

    let path = '/accountNode/editContactProfile' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&contactUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(profile.fullName) +
        '&email=' + encodeURIComponent(profile.email) +
        '&phone=' + encodeURIComponent(profile.phone)

    const timezone = profile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    if (requestReturn) path += '&requestReturn=true'

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

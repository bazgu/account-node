var Log = require('./Log.js')

module.exports = (chooseSessionNode, tokens) => {

    var indexes = Object.create(null)
    tokens.forEach(token => {

        var node = chooseSessionNode.fromSessionToken(token)
        if (node === undefined) return

        var index = node.index
        var set = indexes[index]
        if (set === undefined) {
            set = indexes[index] = {
                node: node,
                tokens: [],
            }
        }
        set.tokens.push(token)

    })

    Object.keys(indexes).forEach(index => {

        var set = indexes[index]

        var path = '/accountNode/close' +
            '?tokens=' + encodeURIComponent(set.tokens.join(','))

        set.node.fetchJson(path, response => {
            if (response === true) return
            Log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
        }, () => {})

    })

}

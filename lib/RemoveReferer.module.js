module.exports = app => (chooseAccountNode, contactUsername, username, publicProfile) => {

    // TODO
    const logPrefix = ''

    let path = '/accountNode/removeReferer' +
        '?username=' + encodeURIComponent(contactUsername) +
        '&refererUsername=' + encodeURIComponent(username) +
        '&fullName=' + encodeURIComponent(publicProfile.fullName) +
        '&email=' + encodeURIComponent(publicProfile.email) +
        '&phone=' + encodeURIComponent(publicProfile.phone)

    const timezone = publicProfile.timezone
    if (timezone !== null) path += '&timezone=' + timezone

    const fetchJson = chooseAccountNode.fromUsername(contactUsername)
    fetchJson(path, response => {
        if (response === true) return
        app.log.error(logPrefix + 'Invalid response: ' + JSON.stringify(response))
    }, () => {})

}

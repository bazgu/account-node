module.exports = object => {
    const plain = Object.create(null)
    for (const i in object) plain[i] = object[i]
    return plain
}

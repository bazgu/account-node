const fs = require('fs')

module.exports = app => (chooseAccountNode, chooseSessionNode,
    users, lowerUsers, contactUsers, requestUsers) => {

    function getGroup (identifier) {
        let group = sessionGroups[identifier]
        if (group === undefined) {
            group = sessionGroups[identifier] = app.SessionGroup(chooseSessionNode, identifier)
        }
        return group
    }

    const sessionGroups = Object.create(null)

    fs.readdirSync('dump').forEach(username => {

        if (username === '.gitignore') return

        const content = fs.readFileSync('dump/' + username, 'utf8')
        const user = app.RestoreUser(chooseAccountNode, chooseSessionNode,
            getGroup, username, JSON.parse(content))
        users[username] = user
        lowerUsers[username.toLowerCase()] = user

        const contacts = user.getContacts()
        for (const i in contacts) contactUsers.put(i, user)

        const requests = user.getRequests()
        for (const i in requests) requestUsers.put(i, user)

    })

}

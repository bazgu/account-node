module.exports = app => loadedConfig => {

    const captchaNodes = loadedConfig.captchaNodes.map((captchaNode, index) => {
        return {
            index: index,
            fetchJson: app.FetchJson(captchaNode.host, captchaNode.port),
        }
    })

    return token => {
        if (token === undefined) return
        const index = parseInt(token.split('_', 1)[0], 10)
        if (!isFinite(index) || index < 0 || index > captchaNodes.length - 1) return
        return captchaNodes[index]
    }

}

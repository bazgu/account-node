module.exports = app => loadedConfig => {

    const sessionNodes = loadedConfig.sessionNodes.map((sessionNode, index) => {

        const host = sessionNode.host,
            port = sessionNode.port

        return {
            index: index,
            host: host,
            port: port,
            logPrefix: 'session-node: ' + host + ':' + port + ': ',
            fetchJson: app.FetchJson(host, port),
        }

    })

    return {
        fromFileToken: token => {
            const index = parseInt(token.split('_', 2)[1], 10)
            if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
            return sessionNodes[index]
        },
        fromSessionToken: token => {
            if (token === undefined) return
            const index = parseInt(token.split('_', 1)[0], 10)
            if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
            return sessionNodes[index]
        },
        random: () => {
            return sessionNodes[Math.floor(Math.random() * sessionNodes.length)]
        },
    }

}

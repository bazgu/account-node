const http = require('http')

module.exports = app => (host, port) => {
    return (path, doneCallback, errorCallback) => {

        const logPrefix = 'fetchJson http://' + host + ':' + port + path + ' '

        app.log.info(logPrefix)
        const req = http.get({
            host: host,
            port: port,
            path: path,
        }, res => {

            const statusCode = res.statusCode
            if (statusCode !== 200) {
                app.log.error(logPrefix + 'HTTP status code ' + statusCode)
                req.abort()
                errorCallback()
                return
            }

            app.ReadText(res, responseText => {

                let response
                try {
                    response = JSON.parse(responseText)
                } catch (e) {
                    app.log.error(logPrefix + 'Invalid JSON document: ' + JSON.stringify(responseText))
                    errorCallback()
                    return
                }

                doneCallback(response)

            })

        })
        req.on('error', err => {
            app.log.error(logPrefix + err.code)
            errorCallback()
        })

        return () => {
            req.abort()
        }

    }
}

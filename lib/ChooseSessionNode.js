var FetchJson = require('./FetchJson.js')

module.exports = loadedConfig => {

    var sessionNodes = loadedConfig.sessionNodes.map((sessionNode, index) => {

        var host = sessionNode.host,
            port = sessionNode.port

        return {
            index: index,
            host: host,
            port: port,
            logPrefix: 'session-node-client: ' + host + ':' + port + ': ',
            fetchJson: FetchJson(host, port),
        }

    })

    return {
        fromFileToken: token => {
            var index = parseInt(token.split('_', 2)[1], 10)
            if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
            return sessionNodes[index]
        },
        fromSessionToken: token => {
            if (token === undefined) return
            var index = parseInt(token.split('_', 1)[0], 10)
            if (!isFinite(index) || index < 0 || index > sessionNodes.length - 1) return
            return sessionNodes[index]
        },
        random: () => {
            return sessionNodes[Math.floor(Math.random() * sessionNodes.length)]
        },
    }

}
